using ApiServicio.ContextoBD;
using ApiServicio.Repositorio;
using ApiServicio.Servicio;
using ApiServicio.Tarea;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Quartz;
using System;

namespace ApiServicio
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<ApiContext>(options =>
                                   options.UseSqlServer(Configuration.GetConnectionString("ServiceDatabase")));

            services.AddTransient<IServicioProceso, CServicioProceso>();
            services.AddTransient<IServicioProcesoA25, CServicioProcesoA25>();

            services.AddScoped<IExamenesRepository, ExamenesRepository>();
            services.AddScoped<ISedes, SedesRepository>();
            services.AddScoped<IParametrosRepository, ParametrosRepository>();
            services.AddScoped<IA25Repository, A25Repository>();
            services.AddScoped<IActExaLabFechaRepository, ActExaLabFechaRepository>();
            services.AddScoped<IItemExamLabRepository, CItemExamLabRepository>();
            services.AddScoped<IA25ProcesarExamenes, CA25ProcesarExamenes>();
            services.AddScoped<IExamenesA25Repository, ExamenesA25Repository>();
            services.AddScoped<IProcesosRepository, ProcesosRepository>();
            services.AddScoped<ICargasRepository, CargasRepository>();
            services.AddScoped<IExamenesCargadosRepository, ExamenesCargadosRepository>();
            services.AddScoped<IRaytoRepositorio, CRaytoRepositorio>();
            services.AddScoped<IRaytoProceso, CRaytoProceso>();
            services.AddScoped<ICrearPlano, CCrearPlano>();

              services.AddQuartz(q =>
            {
                // handy when part of cluster or you want to otherwise identify multiple schedulers
                q.SchedulerId = "Scheduler-Core";

                 q.UseMicrosoftDependencyInjectionScopedJobFactory();

                // these are the defaults
                q.UseSimpleTypeLoader();
                q.UseInMemoryStore();
                q.UseDefaultThreadPool(tp =>
                {
                    tp.MaxConcurrency = 10;
                });

                // configure jobs with code
                var jobKey = new JobKey("awesome job", "awesome group");
                q.AddJob<EjecutarTarea>(j => j
                    .StoreDurably()
                    .WithIdentity(jobKey)
                    .WithDescription("my awesome job")
                );

                q.AddTrigger(t => t
                    .WithIdentity("Simple Trigger")
                    .ForJob(jobKey)
                    .StartNow()
                    .WithSimpleSchedule(x => x.WithInterval(TimeSpan.FromSeconds(5)).RepeatForever())
                    .WithDescription("my awesome simple trigger")
                );

                services.AddQuartzHostedService(options =>
                {
                    // when shutting down we want jobs to complete gracefully
                    options.WaitForJobsToComplete = true;
                });

            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
