﻿using ApiServicio.Entidades;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public interface IParametrosRepository
    {
        ValueTask<List<GeTparametros>> ConsultarParametros();
        ValueTask ActualizarParametro(GeTparametros input);
        ValueTask<GeTparametros> consultarId(int id);
        ValueTask<GeTparametros> ObtenerParametroPorNombre(string NombreParametro);
        ValueTask ActualizarParametros(List<GeTparametros> input);
        ValueTask<GeTparametros> ConsultarParametroPorNombre(string NombreParametro);
        ValueTask<bool> SpFechaSistema();
    }
}
