﻿using ApiServicio.ContextoBD;
using ApiServicio.Dto;
using ApiServicio.Entidades;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public class ExamenesRepository : IExamenesRepository
    {
        private readonly ApiContext _Contex;

        public ExamenesRepository(ApiContext _Contex)
        {
            this._Contex = _Contex;
        }

        public async ValueTask<GeTexamenes> GetId(string id)
        {
            var objeto = await _Contex.GeTexamenes.FirstOrDefaultAsync(x => x.CodId == id);
            return objeto;
        }

        public async ValueTask guardar(GeTexamenes obj)
        {
            _Contex.GeTexamenes.AddRange(obj);
            await _Contex.SaveChangesAsync();
        }

        
        public async ValueTask<List<DTOExamProcesados>> listExamProc(dtoExamenes input)
        {
            try
            {
                var query = @" SELECT a.codId examen,
                                   a.paciente,
	                               a.fecha,protocolo_codigo protocolo,
                                   a.protocolo_titulo titulo,
	                               b.idExamA25 examenA25,
	                               c.tipoMuestra tipoMuestra,
	                               c.resultado resultado,
	                               c.unidades unidades ,
	                               c.fecha fechaA25,
	                               c.hora horaA25
                            FROM [dbo].[GE_TEXAMENES] a
                            INNER JOIN [dbo].[GE_THOMOLOGACIONES] b ON a.protocolo_codigo = b.idExamSofia
                            INNER JOIN [dbo].[GE_TA25] c ON b.idExamA25 = c.tecnica AND RIGHT(a.tubo,4) = c.idPaciente and a.fecha = c.fecha
                            WHERE a.fecha = '" + input.fecha + "'";
                List<DTOExamProcesados> dTOExamProcesados = await _Contex.ExamProcesados.FromSqlRaw(query).ToListAsync();
                var result = dTOExamProcesados;
                return result;
            }
            catch
            {
                throw;
            }

            return null;

        }

        public async ValueTask<List<GeTexamenes>> GetAllFecha(dtoExamenes input)
        {
            try
            {
                var lstExamenes = await _Contex.GeTexamenes.Where(x => x.Fecha == input.fecha && x.Sede == input.sede).ToListAsync();
                return lstExamenes;
            }
            catch
            {
                throw;
            }
        }

        public async ValueTask actualizar(GeTexamenes obj)
        {
            _Contex.GeTexamenes.UpdateRange(obj);
            await _Contex.SaveChangesAsync();
        }

        public async ValueTask<List<GeTexamenes>> PostExamenes(dtoExamenes input)
        {
            try
            {
                var lstExamenes = await _Contex.GeTexamenes.Where(x => x.Fecha == input.fecha && x.Sede == input.sede).ToListAsync();
                return lstExamenes;
            }
            catch
            {
                throw;
            }

        }

        public async ValueTask<List<GeTexamenes>> CedulaFecha(dtoExamenes input)
        {
            try
            {
                var lstExamenes = await _Contex.GeTexamenes.Where(x => x.Paciente == input.paciente && x.Fecha == input.fecha).ToListAsync();
                return lstExamenes;
            }
            catch
            {
                throw;
            }

        }

        public async ValueTask<List<DTORaytoExamenes>> listExamRayto(dtoExamenes input)
        {
            try
            {
                var query = @" SELECT DISTINCT a.codId examen,
		                            a.paciente,
		                            a.fecha,a.hora,protocolo_codigo protocolo,
		                            a.protocolo_titulo titulo
		                            ,[wbc] ,[lym1],[mid1]
		                            ,[gra1],[lym2],[mid2]
		                            ,[gra2],[rbc],[hgb]
		                            ,[mchc],[mch],[mcv]
		                            ,[rdw_cv],[rdw_sd],[hct]
		                            ,[plt],[mpv],[pdw]
		                            ,[pct],[p_lcr]
                                    ,RIGHT(a.tubo,4) tubo
                            FROM [dbo].[GE_TEXAMENES] a
                            inner join [dbo].[GE_TRAYTO] b on CAST(a.tubo AS INT) = b.paciente 
                            left join [dbo].[ITEM_EXAMENLAB] c on a.[codId] = c.[idExamen]
                            left join [dbo].[ACT_EXAMENLAB_FECHA] d on c.idExamen = d.idExamen
                            WHERE a.fecha = '" + input.fecha + "' and a.sede = '" + input.sede + @"' and (d.cargado is null or d.cargado = 'N') 
                                and a.protocolo_titulo like 'HEMOGRAMA' and b.estado = 0  
                            ORDER BY EXAMEN DESC";
                List<DTORayto> dTOExamProcesados = await _Contex.Rayto.FromSqlRaw(query).ToListAsync();
                List<DTORaytoExamenes> lstExam = new List<DTORaytoExamenes>();
                foreach (DTORayto item in dTOExamProcesados)
                {
                    #region
                    DTORaytoExamenes obj = new DTORaytoExamenes();
                    obj.examen = item.examen;
                    obj.paciente = item.paciente;
                    obj.fecha = item.fecha;
                    obj.protocolo = item.protocolo;
                    obj.titulo = item.titulo;
                    obj.codMuestra = "";
                    obj.hora = item.hora;

                    obj.muestra = "WBC";
                    obj.valor = item.wbc;
                    obj.tubo = item.tubo;
                    lstExam.Add(obj);


                    DTORaytoExamenes obj1 = new DTORaytoExamenes();
                    obj1.examen = item.examen;
                    obj1.paciente = item.paciente;
                    obj1.fecha = item.fecha;
                    obj1.protocolo = item.protocolo;
                    obj1.titulo = item.titulo;
                    obj1.codMuestra = "";
                    obj1.hora = item.hora;
                    obj1.muestra = "LYM#";
                    obj1.valor = item.lym1;
                    obj1.tubo = item.tubo;
                    lstExam.Add(obj1);

                    DTORaytoExamenes obj2 = new DTORaytoExamenes();
                    obj2.examen = item.examen;
                    obj2.paciente = item.paciente;
                    obj2.fecha = item.fecha;
                    obj2.protocolo = item.protocolo;
                    obj2.titulo = item.titulo;
                    obj2.codMuestra = "";
                    obj2.hora = item.hora;
                    obj2.muestra = "MID#";
                    obj2.valor = item.mid1;
                    obj2.tubo = item.tubo;
                    lstExam.Add(obj2);

                    DTORaytoExamenes obj3 = new DTORaytoExamenes();
                    obj3.examen = item.examen;
                    obj3.paciente = item.paciente;
                    obj3.fecha = item.fecha;
                    obj3.protocolo = item.protocolo;
                    obj3.titulo = item.titulo;
                    obj3.hora = item.hora;
                    obj3.codMuestra = "";
                    obj3.muestra = "GRA#";
                    obj3.valor = item.gra1;
                    obj3.tubo = item.tubo;
                    lstExam.Add(obj3);

                    DTORaytoExamenes obj4 = new DTORaytoExamenes();
                    obj4.examen = item.examen;
                    obj4.paciente = item.paciente;
                    obj4.fecha = item.fecha;
                    obj4.protocolo = item.protocolo;
                    obj4.titulo = item.titulo;
                    obj4.hora = item.hora;
                    obj4.codMuestra = "";
                    obj4.muestra = "LYM%";
                    obj4.valor = item.lym2;
                    obj4.tubo = item.tubo;
                    lstExam.Add(obj4);

                    DTORaytoExamenes obj5 = new DTORaytoExamenes();
                    obj5.examen = item.examen;
                    obj5.paciente = item.paciente;
                    obj5.fecha = item.fecha;
                    obj5.protocolo = item.protocolo;
                    obj5.titulo = item.titulo;
                    obj5.hora = item.hora;
                    obj5.codMuestra = "";
                    obj5.muestra = "MID%";
                    obj5.valor = item.mid2;
                    obj5.tubo = item.tubo;
                    lstExam.Add(obj5);

                    DTORaytoExamenes obj6 = new DTORaytoExamenes();
                    obj6.examen = item.examen;
                    obj6.paciente = item.paciente;
                    obj6.fecha = item.fecha;
                    obj6.protocolo = item.protocolo;
                    obj6.titulo = item.titulo;
                    obj6.hora = item.hora;
                    obj6.codMuestra = "";
                    obj6.muestra = "GRA%";
                    obj6.valor = item.gra2;
                    obj6.tubo = item.tubo;
                    lstExam.Add(obj6);

                    DTORaytoExamenes obj7 = new DTORaytoExamenes();
                    obj7.examen = item.examen;
                    obj7.paciente = item.paciente;
                    obj7.fecha = item.fecha;
                    obj7.protocolo = item.protocolo;
                    obj7.titulo = item.titulo;
                    obj7.hora = item.hora;
                    obj7.codMuestra = "";
                    obj7.muestra = "RBC";
                    obj7.valor = item.rbc;
                    obj7.tubo = item.tubo;
                    lstExam.Add(obj7);

                    DTORaytoExamenes obj8 = new DTORaytoExamenes();
                    obj8.examen = item.examen;
                    obj8.paciente = item.paciente;
                    obj8.fecha = item.fecha;
                    obj8.protocolo = item.protocolo;
                    obj8.titulo = item.titulo;
                    obj8.hora = item.hora;
                    obj8.codMuestra = "";
                    obj8.muestra = "HGB";
                    obj8.valor = item.hgb;
                    obj8.tubo = item.tubo;
                    lstExam.Add(obj8);

                    DTORaytoExamenes obj9 = new DTORaytoExamenes();
                    obj9.examen = item.examen;
                    obj9.paciente = item.paciente;
                    obj9.fecha = item.fecha;
                    obj9.protocolo = item.protocolo;
                    obj9.titulo = item.titulo;
                    obj9.hora = item.hora;
                    obj9.codMuestra = "";
                    obj9.muestra = "MCHC";
                    obj9.valor = item.mchc;
                    obj9.tubo = item.tubo;
                    lstExam.Add(obj9);

                    DTORaytoExamenes obj10 = new DTORaytoExamenes();
                    obj10.examen = item.examen;
                    obj10.paciente = item.paciente;
                    obj10.fecha = item.fecha;
                    obj10.protocolo = item.protocolo;
                    obj10.titulo = item.titulo;
                    obj10.hora = item.hora;
                    obj10.codMuestra = "";
                    obj10.muestra = "MCH";
                    obj10.valor = item.mch;
                    obj10.tubo = item.tubo;
                    lstExam.Add(obj10);

                    DTORaytoExamenes obj11 = new DTORaytoExamenes();
                    obj11.examen = item.examen;
                    obj11.paciente = item.paciente;
                    obj11.fecha = item.fecha;
                    obj11.protocolo = item.protocolo;
                    obj11.titulo = item.titulo;
                    obj11.hora = item.hora;
                    obj11.codMuestra = "";
                    obj11.muestra = "MCV";
                    obj11.valor = item.mcv;
                    obj11.tubo = item.tubo;
                    lstExam.Add(obj11);

                    DTORaytoExamenes obj12 = new DTORaytoExamenes();
                    obj12.examen = item.examen;
                    obj12.paciente = item.paciente;
                    obj12.fecha = item.fecha;
                    obj12.protocolo = item.protocolo;
                    obj12.titulo = item.titulo;
                    obj12.hora = item.hora;
                    obj12.codMuestra = "";
                    obj12.muestra = "RDW-CV";
                    obj12.valor = item.rdw_cv;
                    obj12.tubo = item.tubo;
                    lstExam.Add(obj12);

                    DTORaytoExamenes obj13 = new DTORaytoExamenes();
                    obj13.examen = item.examen;
                    obj13.paciente = item.paciente;
                    obj13.fecha = item.fecha;
                    obj13.protocolo = item.protocolo;
                    obj13.titulo = item.titulo;
                    obj13.hora = item.hora;
                    obj13.codMuestra = "";
                    obj13.muestra = "RDW-SD";
                    obj13.valor = item.rdw_sd;
                    obj13.tubo = item.tubo;
                    lstExam.Add(obj13);

                    DTORaytoExamenes obj14 = new DTORaytoExamenes();
                    obj14.examen = item.examen;
                    obj14.paciente = item.paciente;
                    obj14.fecha = item.fecha;
                    obj14.protocolo = item.protocolo;
                    obj14.titulo = item.titulo;
                    obj14.hora = item.hora;
                    obj14.codMuestra = "";
                    obj14.muestra = "HCT";
                    obj14.valor = item.hct;
                    obj14.tubo = item.tubo;
                    lstExam.Add(obj14);

                    DTORaytoExamenes obj15 = new DTORaytoExamenes();
                    obj15.examen = item.examen;
                    obj15.paciente = item.paciente;
                    obj15.fecha = item.fecha;
                    obj15.protocolo = item.protocolo;
                    obj15.titulo = item.titulo;
                    obj15.hora = item.hora;
                    obj15.codMuestra = "";
                    obj15.muestra = "PLT";
                    obj15.valor = item.plt;
                    obj15.tubo = item.tubo;
                    lstExam.Add(obj15);

                    DTORaytoExamenes obj16 = new DTORaytoExamenes();
                    obj16.examen = item.examen;
                    obj16.paciente = item.paciente;
                    obj16.fecha = item.fecha;
                    obj16.protocolo = item.protocolo;
                    obj16.titulo = item.titulo;
                    obj16.hora = item.hora;
                    obj16.codMuestra = "";
                    obj16.muestra = "MPV";
                    obj16.valor = item.mpv;
                    obj16.tubo = item.tubo;
                    lstExam.Add(obj16);

                    DTORaytoExamenes obj17 = new DTORaytoExamenes();
                    obj17.examen = item.examen;
                    obj17.paciente = item.paciente;
                    obj17.fecha = item.fecha;
                    obj17.protocolo = item.protocolo;
                    obj17.titulo = item.titulo;
                    obj17.hora = item.hora;
                    obj17.codMuestra = "";
                    obj17.muestra = "PDW";
                    obj17.valor = item.pdw;
                    obj17.tubo = item.tubo;
                    lstExam.Add(obj17);

                    DTORaytoExamenes obj18 = new DTORaytoExamenes();
                    obj18.examen = item.examen;
                    obj18.paciente = item.paciente;
                    obj18.fecha = item.fecha;
                    obj18.protocolo = item.protocolo;
                    obj18.titulo = item.titulo;
                    obj18.hora = item.hora;
                    obj18.codMuestra = "";
                    obj18.muestra = "PCT";
                    obj18.valor = item.pct;
                    obj18.tubo = item.tubo;
                    lstExam.Add(obj18);

                    DTORaytoExamenes obj19 = new DTORaytoExamenes();
                    obj19.examen = item.examen;
                    obj19.paciente = item.paciente;
                    obj19.fecha = item.fecha;
                    obj19.protocolo = item.protocolo;
                    obj19.titulo = item.titulo;
                    obj19.hora = item.hora;
                    obj19.codMuestra = "";
                    obj19.muestra = "P-LCR";
                    obj19.valor = item.p_lcr;
                    obj19.tubo = item.tubo;
                    lstExam.Add(obj19);
                    #endregion
                }

                var result = lstExam;
                return result;
            }
            catch
            {
                throw;
            }

        }

        public async ValueTask<List<DTOExamProcesados>> listExamPerfil(dtoExamenes input)
        {
            try
            {
                var query = @" SELECT a.codId examen,
                            a.paciente,
	                        a.fecha,protocolo_codigo protocolo,
                            d.muestras titulo ,
	                        c.tipoMuestra tipoMuestra,
	                        c.resultado resultado,
	                        c.unidades unidades ,
	                        c.fecha fechaA25,
	                        c.hora horaA25,
                            '' as examenA25
                    FROM [dbo].[GE_TEXAMENES] a
                    INNER JOIN [dbo].[GE_TPERFIL] d ON a.protocolo_titulo = d.protocolo
                    INNER JOIN [dbo].[GE_TA25] c ON  d.tecnicaA25 = c.tecnica AND RIGHT(a.tubo,4) = c.idPaciente and a.fecha = c.fecha 
                    WHERE a.fecha = '" + input.fecha + "' and a.protocolo_titulo = 'PERFIL LIPIDICO'";
                List<DTOExamProcesados> dTOExamProcesados = await _Contex.ExamProcesados.FromSqlRaw(query).ToListAsync();
                var result = dTOExamProcesados;
                return result;
            }
            catch
            {
                throw;
            }
        }

        public async ValueTask<List<DTOHemograma>> listHemogramas(dtoExamenes input)
        {
            try
            {
                var query = @" SELECT distinct a.codId examen,
		                            a.paciente,
		                            a.nombre,
		                            a.fecha,	
		                            a.hora,
		                            protocolo_codigo protocolo,
		                            a.protocolo_titulo descripcion
                                    ,RIGHT(a.tubo,4) tubo
                                    , 0 carga
                            FROM [dbo].[GE_TEXAMENES] a
                            inner join [dbo].[GE_TRAYTO] b on CAST(a.tubo AS INT) = b.paciente
                            inner join [dbo].[ITEM_EXAMENLAB] c on a.[codId] = c.[idExamen]
                            inner join [dbo].[ACT_EXAMENLAB_FECHA] d on c.idExamen = d.idExamen
                            WHERE a.fecha = '" + input.fecha + "' and a.sede = '" + input.sede + "' and d.cargado = 'N'  and a.protocolo_titulo = 'HEMOGRAMA' and b.estado = 0 ";
                List<DTOHemograma> dTOHemograma = await _Contex.Hemograma.FromSqlRaw(query).ToListAsync();
                var result = dTOHemograma;
                return result;
            }
            catch
            {
                throw;
            }
        }
    }
}
