﻿using ApiServicio.Dto;
using ApiServicio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public interface IRaytoRepositorio
    {
        ValueTask Guardar(List<GeTrayto> ListaDatos);
        ValueTask Actualizar(GeTrayto obj);
        ValueTask<List<DTORaytoExamenes>> listExamRayto(string fecha);
        ValueTask<List<GeTrayto>> get();
        ValueTask<bool> GetPaciente(string paciente, string fecha);
        ValueTask Inactivar(string id);
        ValueTask<List<GeTrayto>> getFecha(string fecha, string sede);
    }
}
