﻿using ApiServicio.Dto;
using ApiServicio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public interface IExamenesA25Repository
    {
        ValueTask<List<GeTexamenesA25>> Get();
        ValueTask<GeTexamenesA25> GetId(int id);
        ValueTask<GeTexamenesA25> Examenes(DTOInput obj);
        ValueTask guardar(GeTexamenesA25 obj);
        ValueTask actualizar(GeTexamenesA25 obj);
    }
}
