﻿using ApiServicio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public interface ICargasRepository
    {
        ValueTask<List<GeTcargas>> GetAll();
        ValueTask<List<GeTcargas>> cargas(int idProceso);
        ValueTask<GeTcargas> GetId(int id);
        ValueTask guardar(GeTcargas obj);
        ValueTask actualizar(GeTcargas obj);
    }
}
