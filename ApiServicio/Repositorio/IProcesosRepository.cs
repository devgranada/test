﻿using ApiServicio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public interface IProcesosRepository
    {
        ValueTask<GeTprocesos> GetId(int id);
        ValueTask<List<GeTprocesos>> GetProcSedeFecha(string procNombre, string sede, DateTime fecha);
        ValueTask<List<GeTprocesos>> GetProcSedeFecha(string procNombre, string sede);
        ValueTask<List<GeTprocesos>> GetProcSedeFecha(string procNombre);
        ValueTask guardar(GeTprocesos obj);
        ValueTask actualizar(GeTprocesos obj);
        ValueTask<List<GeTprocesos>> GetEstadoN();
        ValueTask<List<GeTprocesos>> GetEstadoS();
    }
}
