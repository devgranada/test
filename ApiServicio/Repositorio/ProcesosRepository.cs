﻿using ApiServicio.ContextoBD;
using ApiServicio.Entidades;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public class ProcesosRepository : IProcesosRepository
    {
        private readonly ApiContext _context;

        public ProcesosRepository(ApiContext _context)
        {
            this._context = _context;
        }

        public async ValueTask<List<GeTprocesos>> GetEstadoN()
        {
            var objeto = await _context.GeTprocesos.Where(x => x.Cargados == "N").ToListAsync();
            return objeto;
        }

        public async ValueTask<List<GeTprocesos>> GetEstadoS()
        {
            var objeto = await _context.GeTprocesos.Where(x => x.Cargados == "S").ToListAsync();
            return objeto;
        }

        public async ValueTask<GeTprocesos> GetId(int id)
        {
            var objeto = await _context.GeTprocesos.FirstOrDefaultAsync(x => x.Id == id);
            return objeto;
        }

        public async ValueTask<List<GeTprocesos>> GetProcSedeFecha(string procNombre, string sede, DateTime fecha)
        {
            var objeto = await _context.GeTprocesos.Where(x => x.Nombre == procNombre & x.Sede == sede & x.Fecha == fecha).ToListAsync();
            return objeto;
        }

        public async ValueTask<List<GeTprocesos>> GetProcSedeFecha(string procNombre, string sede)
        {
            var objeto = await _context.GeTprocesos.Where(x => x.Nombre == procNombre & x.Sede == sede).ToListAsync();
            return objeto;
        }

        public async ValueTask<List<GeTprocesos>> GetProcSedeFecha(string procNombre)
        {
            var objeto = await _context.GeTprocesos.Where(x => x.Nombre == procNombre).ToListAsync();
            return objeto;
        }

        public async ValueTask guardar(GeTprocesos obj)
        {
            _context.GeTprocesos.AddRange(obj);
            await _context.SaveChangesAsync();
        }

        public async ValueTask actualizar(GeTprocesos obj)
        {
            _context.GeTprocesos.UpdateRange(obj);
            await _context.SaveChangesAsync();
        }

    }
}
