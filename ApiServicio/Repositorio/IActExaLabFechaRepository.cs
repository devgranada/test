﻿using ApiServicio.Dto;
using ApiServicio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public interface IActExaLabFechaRepository
    {
        ValueTask<List<ActExamenlabFecha>> Get(string fecha, string sede);
        ValueTask<ActExamenlabFecha> ById(string id);
        ValueTask<ActExamenlabFecha> IdCabecera(DTOActExaLabFec obj);
        ValueTask guardar(ActExamenlabFecha obj);
        ValueTask actualizar(ActExamenlabFecha obj);
        ValueTask<ActExamenlabFecha> GetExamenPaciente(DTOActExaLabFec obj);
        ValueTask<List<ActExamenlabFecha>> GetAllHemogramas(string fecha, string sede);
    }
}
