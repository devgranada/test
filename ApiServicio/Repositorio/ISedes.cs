﻿using ApiServicio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public interface ISedes
    {
        ValueTask<List<GeTsedes>> GetAll();
        ValueTask<GeTsedes> GetId(int id);
        ValueTask<GeTsedes> GetId(string sede);
    }
}
