﻿using ApiServicio.Dto;
using ApiServicio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public interface IItemExamLabRepository
    {
        ValueTask<List<ItemExamenlab>> Get();
        ValueTask<ItemExamenlab> Get(string id);
        ValueTask adicionar(ItemExamenlab obj);
        ValueTask actualizar(ItemExamenlab obj);
        ValueTask<ItemExamenlab> GetByExamen(DTOItemExamen obj);
        ValueTask<List<ItemExamenlab>> lstExamen(DTOItemExamen obj);
        ValueTask<List<ItemExamenlab>> lstExamenHemograma(DTOItemExamen obj);
        ValueTask<List<DTOProcesadosA25>> lstProcesadosA25(DTOItemExamen obj);
        ValueTask<List<DTOExamenesPro>> lstCarga(DTOItemExamen obj);
        ValueTask<decimal> lstColesterolLDL(DTOColesterolLDL obj);
    }
}
