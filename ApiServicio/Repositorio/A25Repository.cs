﻿using ApiServicio.ContextoBD;
using ApiServicio.Entidades;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public class A25Repository : IA25Repository
    {
        private readonly ApiContext _context;

        public A25Repository(ApiContext _context)
        {
            this._context = _context;
        }

        public async ValueTask Guardar(List<GeTa25> ListaDatos)
        {
            await _context.GeTa25.AddRangeAsync(ListaDatos);
            await _context.SaveChangesAsync();
        }

        public async ValueTask<bool> ConsultarExiste(GeTa25 ObjConsulta)
        {
            var Existe = await _context.GeTa25.AnyAsync(x => x.IdPaciente == ObjConsulta.IdPaciente
                                        && x.Fecha == ObjConsulta.Fecha
                                        && x.Tecnica == ObjConsulta.Tecnica);
            return Existe;
        }

        public async ValueTask<GeTa25> GetId(string id)
        {
            var objeto = await _context.GeTa25.FirstOrDefaultAsync(x => x.IdPaciente == id);
            return objeto;
        }

        public async ValueTask Actualizar(GeTa25 obj)
        {
             _context.GeTa25.UpdateRange(obj);
            await _context.SaveChangesAsync();
        }

        public async ValueTask<List<GeTa25>> GetIdFecha(string fecha)
        {
            try
            {
                var lstResult = await _context.GeTa25
                                        .Where(x => x.Fecha == fecha).ToListAsync();
                return lstResult;
            }
            catch
            {
                throw;
            }

        }

    }
}
