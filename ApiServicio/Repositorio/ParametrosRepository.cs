﻿using ApiServicio.ContextoBD;
using ApiServicio.Entidades;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public class ParametrosRepository : IParametrosRepository
    {
        private readonly ApiContext _context;

        public ParametrosRepository(ApiContext context)
        {
            this._context = context;
        }

        public async ValueTask<List<GeTparametros>> ConsultarParametros()
        {
            var result = await _context.GeTparametros.ToListAsync();
            return result;
        }

        public async ValueTask<GeTparametros> consultarId(int id)
        {
            var result = await _context.GeTparametros.FirstOrDefaultAsync(x => x.Id == id);
            return result;
        }

        public async ValueTask<GeTparametros> ConsultarParametroPorNombre(string NombreParametro)
        {
            var result = await _context.GeTparametros.FirstOrDefaultAsync(x => x.Llave == NombreParametro && x.Estado == 1);
            return result;
        }

        public async ValueTask ActualizarParametro(GeTparametros input)
        {
            _context.GeTparametros.Update(input);
            await _context.SaveChangesAsync();
        }

        public async ValueTask ActualizarParametros(List<GeTparametros> input)
        {
            _context.GeTparametros.UpdateRange(input);
            await _context.SaveChangesAsync();
        }

        public async ValueTask<GeTparametros> ObtenerParametroPorNombre(string NombreParametro)
        {
            var Parametro = await _context.GeTparametros.FirstOrDefaultAsync(a => a.Llave == NombreParametro && a.Estado == 1);
            return Parametro;
        }

        public async ValueTask<bool> SpFechaSistema()
        {
            try
            {
                bool op = false;
                StringBuilder consulta = new StringBuilder();
                consulta.AppendLine("EXECUTE [sp_fechaSistema]");

                using (var command = _context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "sp_fechaSistema";
                    command.CommandType = CommandType.StoredProcedure;
                    _context.Database.OpenConnection();
                    using (var result = await command.ExecuteReaderAsync())
                    {
                        if (result.Read())
                        {
                            op = true;
                        }
                    }
                    _context.Database.CloseConnection();
                }
                return op;
            }
            catch
            {
                throw;
            }
        }
    }
}
