﻿using ApiServicio.ContextoBD;
using ApiServicio.Dto;
using ApiServicio.Entidades;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public class CItemExamLabRepository : IItemExamLabRepository
    {
        private readonly ApiContext _context;

        public CItemExamLabRepository(ApiContext _context)
        {
            this._context = _context;
        }

        public async ValueTask<List<ItemExamenlab>> Get()
        {
            var result = await _context.ItemExamenlab.ToListAsync();
            return result;
        }

        public async ValueTask<ItemExamenlab> Get(string id)
        {
            var result = await _context.ItemExamenlab.FirstOrDefaultAsync(x => x.IdExamen == id);
            return result;
        }

        public async ValueTask adicionar(ItemExamenlab obj)
        {
            _context.ItemExamenlab.AddRange(obj);
            await _context.SaveChangesAsync();
        }

        public async ValueTask actualizar(ItemExamenlab obj)
        {
            _context.ItemExamenlab.UpdateRange(obj);
            await _context.SaveChangesAsync();
        }

        public async ValueTask<ItemExamenlab> GetByExamen(DTOItemExamen obj)
        {
            try
            {
                var result = await _context.ItemExamenlab
                                .FirstOrDefaultAsync(x => x.IdExamen == obj.idExamen
                                        & x.Paciente == obj.paciente
                                        & x.Texto == obj.texto);
                return result;
            }
            catch
            {
                throw;
            }
        }

        public async ValueTask<List<ItemExamenlab>> lstExamen(DTOItemExamen obj)
        {
            try
            {
                var strQuery = @"SELECT a.[id]
                                ,a.[id_cabecera]
                                ,a.[idExamen]
                                ,a.[paciente]
                                ,a.[fecha]
                                ,a.[texto]
                                ,a.[valor_cualitativo]
                                ,a.[valor_referencia]
                                ,a.[valor_adicional]
                            FROM [dbo].[ITEM_EXAMENLAB] a
                            inner join [dbo].[ACT_EXAMENLAB_FECHA] b on a.idExamen = b.idExamen
                            inner join [dbo].[GE_TEXAMENES] x on x.codId = b.idExamen and x.sede ='" + obj.sede + @"'
                            and x.protocolo_titulo != 'HEMOGRAMA'
                            where b.fecha = '" + obj.fecha + "' and b.cargado = 'N'";
                List<ItemExamenlab> lstResult = await _context.ItemExamenlab.FromSqlRaw(strQuery).ToListAsync();
                return lstResult;
            }
            catch
            {
                throw;
            }
        }

        public async ValueTask<List<ItemExamenlab>> lstExamenHemograma(DTOItemExamen obj)
        {
            try
            {
                var strQuery = @"SELECT a.[id]
                                ,a.[id_cabecera]
                                ,a.[idExamen]
                                ,a.[paciente]
                                ,a.[fecha]
                                ,a.[texto]
                                ,a.[valor_cualitativo]
                                ,a.[valor_referencia]
                                ,a.[valor_adicional]
                            FROM [dbo].[ITEM_EXAMENLAB] a
                            inner join [dbo].[ACT_EXAMENLAB_FECHA] b on a.idExamen = b.idExamen
                            inner join [dbo].[GE_TEXAMENES] x on x.codId = b.idExamen and x.sede ='" + obj.sede + @"'
                            and x.protocolo_titulo = 'HEMOGRAMA'
                            where b.fecha = '" + obj.fecha + "' and b.cargado = 'N'";
                List<ItemExamenlab> lstResult = await _context.ItemExamenlab.FromSqlRaw(strQuery).ToListAsync();
                return lstResult;
            }
            catch
            {
                throw;
            }
        }

        public async ValueTask<List<DTOProcesadosA25>> lstProcesadosA25(DTOItemExamen obj)
        {
            try
            {
                var strQuery = @"SELECT convert(varchar(10),a.[id]) id
                            ,a.[codId] examen
                            ,a.[protocolo_titulo] protocolo
                            ,a.[tubo] tubo
                            ,a.[paciente] paciente
                            ,a.[nombre] nombre
	                        ,c.texto prueba
	                        ,c.valor_cualitativo resultado
	                        ,c.valor_referencia referencia
                        FROM [dbo].[GE_TEXAMENES] a 
                        inner join [dbo].[ACT_EXAMENLAB_FECHA] b on a.codId = b.idExamen and a.paciente = b.paciente
                        inner join [dbo].[ITEM_EXAMENLAB] c on b.id = c.id_cabecera and b.idExamen = c.idExamen and b.paciente = c.paciente
                        where a.fecha = '" + obj.fecha + "' and a.protocolo_titulo != 'HEMOGRAMA'";
                List<DTOProcesadosA25> lstResult = await _context.DTOProcesadosA25.FromSqlRaw(strQuery).ToListAsync();
                return lstResult;
            }
            catch
            {
                throw;
            }
        }

        public async ValueTask<List<DTOExamenesPro>> lstCarga(DTOItemExamen obj)
        {
            try
            {
                var strQuery = @"SELECT distinct a.[codId] codigo
                            ,a.[protocolo_titulo] examen
                            ,a.[fecha] fecha
                            ,a.[paciente] cedula
                            ,a.[nombre] nombre
                            ,a.[sede] sede
                            , 0 estado
                        FROM [dbo].[GE_TEXAMENES] a
                        inner join [dbo].[ITEM_EXAMENLAB] b on a.[codId] = b.[idExamen]
                        inner join [dbo].[ACT_EXAMENLAB_FECHA] c on b.idExamen = c.idExamen
                        where a.fecha = '" + obj.fecha + "' and a.sede = '" + obj.sede + "' and (c.cargado is null or c.cargado = 'N') and a.protocolo_titulo != 'HEMOGRAMA' ";
                List<DTOExamenesPro> lstResult = await _context.DTOExamenesPro.FromSqlRaw(strQuery).ToListAsync();
                return lstResult;
            }
            catch
            {
                throw;
            }
        }

        public async ValueTask<decimal> lstColesterolLDL(DTOColesterolLDL obj)
        {
            try
            {
                decimal lnValor1 = 0;
                decimal lnValor2 = 0;
                decimal lnValor3 = 0;
                decimal lnTotal = 0;
                int i = 0;
                var strQuery = @"SELECT a.[id]
                                      ,a.[id_cabecera]
                                      ,a.[idExamen]
                                      ,a.[paciente]
                                      ,a.[fecha]
                                      ,a.[texto]
                                      ,a.[valor_cualitativo]
                                      ,a.[valor_referencia]
                                      ,a.[valor_adicional]
                                    FROM [dbo].[ITEM_EXAMENLAB] a
                                    inner join [dbo].[ACT_EXAMENLAB_FECHA] b on a.idExamen = b.idExamen
                                    where b.fecha = '" + obj.strFecha + "' and b.cargado = 'N' and a.idExamen = " + obj.idExamen + " and " +
                                    " a.texto in ('" + obj.colesterol + "','" + obj.colesterolHDL + "','" + obj.colesterolVLDL + "')";

                List<ItemExamenlab> lstResult = await _context.ItemExamenlab.FromSqlRaw(strQuery).ToListAsync();
                if (lstResult.Count == 3)
                {
                    foreach (ItemExamenlab item in lstResult)
                    {
                        i++;
                        if (item.Texto.Contains("COLESTEROL TOTAL"))
                        {
                            if (item.ValorCualitativo.Contains('*'))
                            {
                                lnValor1 = decimal.Parse(item.ValorCualitativo.Substring(0, item.ValorCualitativo.Length - 1));
                            }
                            else
                            {
                                lnValor1 = decimal.Parse(item.ValorCualitativo);
                            }
                        }
                        if (item.Texto.Contains("COLESTEROL DE ALTA DENSIDAD (HDL)"))
                        {
                            if (item.ValorCualitativo.Contains('*'))
                            {
                                lnValor2 = decimal.Parse(item.ValorCualitativo.Substring(0, item.ValorCualitativo.Length - 1));
                            }
                            else
                            {
                                lnValor2 = decimal.Parse(item.ValorCualitativo);
                            }
                        }
                        if (item.Texto.Contains("COLESTEROL DE MUY BAJA DENSIDAD (VLDL)"))
                        {
                            if (item.ValorCualitativo.Equals("*"))
                            {
                                lnValor3 = 0;
                            }
                            else
                            {
                                lnValor3 = decimal.Parse(item.ValorCualitativo);
                            }
                        }
                    }
                }
                if (lnValor3 == 0)
                {
                    lnTotal = 0;
                }
                else
                {
                    lnTotal = lnValor1 - lnValor2;
                }

                return (lnTotal - lnValor3);
            }
            catch
            {
                throw;
            }
        }

    }
}
