﻿using ApiServicio.Dto;
using ApiServicio.Entidades;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public interface IExamenesRepository
    {
        ValueTask<GeTexamenes> GetId(string id);
        ValueTask guardar(GeTexamenes obj);
        ValueTask<List<DTOExamProcesados>> listExamProc(dtoExamenes input);
        ValueTask<List<GeTexamenes>> GetAllFecha(dtoExamenes input);
        ValueTask actualizar(GeTexamenes obj);
        ValueTask<List<GeTexamenes>> PostExamenes(dtoExamenes input);
        ValueTask<List<GeTexamenes>> CedulaFecha(dtoExamenes input);
        ValueTask<List<DTORaytoExamenes>> listExamRayto(dtoExamenes input);
        ValueTask<List<DTOExamProcesados>> listExamPerfil(dtoExamenes input);
        ValueTask<List<DTOHemograma>> listHemogramas(dtoExamenes input);
    }
}
