﻿using ApiServicio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public interface IA25Repository
    {
        ValueTask Guardar(List<GeTa25> ListaDatos);
        ValueTask<bool> ConsultarExiste(GeTa25 ObjConsulta);
        ValueTask<GeTa25> GetId(string id);
        ValueTask Actualizar(GeTa25 obj);
        ValueTask<List<GeTa25>> GetIdFecha(string fecha);
    }
}
