﻿using ApiServicio.ContextoBD;
using ApiServicio.Entidades;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public class CargasRepository : ICargasRepository
    {
        private readonly ApiContext _context;

        public CargasRepository(ApiContext _context)
        {
            this._context = _context;
        }

        public async ValueTask<List<GeTcargas>> GetAll()
        {
            var obj = await _context.GeTcargas.ToListAsync();
            return obj;
        }

        public async ValueTask<List<GeTcargas>> cargas(int idProceso)
        {
            var obj = await _context.GeTcargas.Where(x => x.IdProceso == idProceso & !x.Respuesta.Equals("S")).ToListAsync();
            return obj;
        }

        public async ValueTask<GeTcargas> GetId(int id)
        {
            var objeto = await _context.GeTcargas.FirstOrDefaultAsync(x => x.Id == id);
            return objeto;
        }

        public async ValueTask guardar(GeTcargas obj)
        {
            _context.GeTcargas.AddRange(obj);
            await _context.SaveChangesAsync();
        }

        public async ValueTask actualizar(GeTcargas obj)
        {
            _context.GeTcargas.UpdateRange(obj);
            await _context.SaveChangesAsync();
        }
    }
}
