﻿using ApiServicio.ContextoBD;
using ApiServicio.Dto;
using ApiServicio.Entidades;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public class ExamenesA25Repository : IExamenesA25Repository
    {
        private readonly ApiContext _context;

        public ExamenesA25Repository(ApiContext _context)
        {
            this._context = _context;
        }

        public async ValueTask<List<GeTexamenesA25>> Get()
        {
            var result = await _context.GeTexamenesA25.ToListAsync();
            return result;
        }

        public async ValueTask<GeTexamenesA25> GetId(int id)
        {
            var objeto = await _context.GeTexamenesA25.FirstOrDefaultAsync(x => x.Id == id);
            return objeto;
        }

        public async ValueTask<GeTexamenesA25> Examenes(DTOInput obj)
        {
            var objeto = await _context.GeTexamenesA25.FirstOrDefaultAsync(x => x.CodA25 == obj.codigo);
            return objeto;
        }

        public async ValueTask guardar(GeTexamenesA25 obj)
        {
            _context.GeTexamenesA25.AddRange(obj);
            await _context.SaveChangesAsync();
        }

        public async ValueTask actualizar(GeTexamenesA25 obj)
        {
            _context.GeTexamenesA25.UpdateRange(obj);
            await _context.SaveChangesAsync();
        }
    }
}
