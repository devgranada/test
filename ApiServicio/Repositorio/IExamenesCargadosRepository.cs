﻿using ApiServicio.Dto;
using ApiServicio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public interface IExamenesCargadosRepository
    {
        ValueTask<List<GeTexamenesCargados>> Get();
        ValueTask<List<GeTexamenesCargados>> GetProceso(int idProceso);
        ValueTask<GeTexamenesCargados> GetId(string id);
        ValueTask<bool> buscarExam(DTOExamCargados obj);
        ValueTask guardar(GeTexamenesCargados obj);
        ValueTask actualizar(GeTexamenesCargados obj);
    }
}
