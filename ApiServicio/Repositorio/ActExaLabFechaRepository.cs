﻿using ApiServicio.ContextoBD;
using ApiServicio.Dto;
using ApiServicio.Entidades;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public class ActExaLabFechaRepository : IActExaLabFechaRepository
    {
        private readonly ApiContext _context;

        public ActExaLabFechaRepository(ApiContext _context)
        {
            this._context = _context;
        }

        public async ValueTask<List<ActExamenlabFecha>> Get(string fecha, string sede)
        {
            /*var objeto = await _context.ACT_EXAMENLAB_FECHA.Where(x => x.fecha == fecha).ToListAsync();
            return objeto;*/
            try
            {
                var query = @"SELECT t.[id]
                          ,t.[idExamen]
                          ,t.[paciente]
                          ,t.[fecha]
                          ,t.[resultado_global]
                          ,t.[responsable]
                          ,t.[nota]
                          ,t.[cargado]
                          ,t.[fecha_carga]
                          ,ISNULL(t.[proceso],0) proceso
                          ,ISNULL(t.fechaProceso, NULL) fechaProceso
                      FROM [dbo].[ACT_EXAMENLAB_FECHA] t 
                      inner join [dbo].[GE_TEXAMENES] x on x.codId = t.idExamen and x.sede ='" + sede + @"'
                      and x.protocolo_titulo != 'HEMOGRAMA'
                      where t.fecha = '" + fecha + "' and (t.cargado is null or t.cargado = 'N')";
                List<ActExamenlabFecha> dTOExamProcesados = await _context.ActExamenlabFecha.FromSqlRaw(query).ToListAsync();
                var result = dTOExamProcesados;
                return result;
            }
            catch
            {
                throw;
            }
        }

        public async ValueTask<List<ActExamenlabFecha>> GetAllHemogramas(string fecha, string sede)
        {
            /*var objeto = await _context.ACT_EXAMENLAB_FECHA.Where(x => x.fecha == fecha).ToListAsync();
            return objeto;*/

            try
            {
                var query = @"SELECT t.[id]
                          ,t.[idExamen]
                          ,t.[paciente]
                          ,t.[fecha]
                          ,t.[resultado_global]
                          ,t.[responsable]
                          ,t.[nota]
                          ,t.[cargado]
                          ,t.[fecha_carga]
                          ,ISNULL(t.[proceso],0) proceso
                          ,ISNULL(t.fechaProceso, NULL) fechaProceso
                      FROM [dbo].[ACT_EXAMENLAB_FECHA] t 
                      inner join [dbo].[GE_TEXAMENES] x on x.codId = t.idExamen and x.sede ='" + sede + @"'
                      and x.protocolo_titulo = 'HEMOGRAMA'
                      where t.fecha = '" + fecha + "' and (t.cargado is null or t.cargado = 'N')";
                List<ActExamenlabFecha> dTOExamProcesados = await _context.ActExamenlabFecha.FromSqlRaw(query).ToListAsync();
                var result = dTOExamProcesados;
                return result;
            }
            catch
            {
                throw;
            }
        }

        public async ValueTask<ActExamenlabFecha> ById(string id)
        {
            try
            {
                var objeto = await _context.ActExamenlabFecha.FirstOrDefaultAsync(x => x.IdExamen == id);
                return objeto;
            }
            catch
            {
                throw;
            }
        }

        public async ValueTask<ActExamenlabFecha> IdCabecera(DTOActExaLabFec obj)
        {
            try
            {
                var objeto = await _context.ActExamenlabFecha.FirstOrDefaultAsync(x => x.IdExamen == obj.examen
                                & x.Paciente == obj.paciente
                                & x.Fecha == obj.fecha);
                return objeto;
            }
            catch
            {
                throw;
            }
        }

        public async ValueTask<ActExamenlabFecha> GetExamenPaciente(DTOActExaLabFec obj)
        {
            try
            {
                var objeto = await _context.ActExamenlabFecha.FirstOrDefaultAsync(x => x.IdExamen == obj.examen
                                & x.Paciente == obj.paciente);
                return objeto;
            }
            catch
            {
                throw;
            }
        }

        public async ValueTask guardar(ActExamenlabFecha obj)
        {
            _context.ActExamenlabFecha.AddRange(obj);
            await _context.SaveChangesAsync();
        }

        public async ValueTask actualizar(ActExamenlabFecha obj)
        {
            _context.ActExamenlabFecha.UpdateRange(obj);
            await _context.SaveChangesAsync();
        }
    }
}
