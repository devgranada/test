﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiServicio.ContextoBD;
using ApiServicio.Dto;
using ApiServicio.Entidades;
using Microsoft.EntityFrameworkCore;

namespace ApiServicio.Repositorio
{
    public class ExamenesCargadosRepository : IExamenesCargadosRepository
    {
        private readonly ApiContext _context;

        public ExamenesCargadosRepository(ApiContext _context)
        {
            this._context = _context;
        }

        public async ValueTask<List<GeTexamenesCargados>> Get()
        {
            var result = await _context.GeTexamenesCargados.ToListAsync();
            return result;
        }

        public async ValueTask<List<GeTexamenesCargados>> GetProceso(int idProceso)
        {
            var result = await _context.GeTexamenesCargados.Where(x => x.IdProceso == idProceso).ToListAsync();
            return result;
        }

        public async ValueTask<GeTexamenesCargados> GetId(string id)
        {
            var obj = await _context.GeTexamenesCargados.SingleOrDefaultAsync(p => p.IdExamenSofia == id);
            return obj;
        }

        public async ValueTask<bool> buscarExam(DTOExamCargados obj)
        {
            GeTexamenesCargados result = await _context.GeTexamenesCargados.SingleOrDefaultAsync(p => p.IdExamenSofia == obj.examen
                               & p.IdPaciente == obj.paciente
                               & p.Tipo == obj.tipo
                               & p.Muestra == obj.muestra);
            
            bool op = false;
            if (result != null)
            {
                op = true;
            }

            return op;
        }

        public async ValueTask guardar(GeTexamenesCargados obj)
        {
            _context.GeTexamenesCargados.AddRange(obj);
            await _context.SaveChangesAsync();
        }

        public async ValueTask actualizar(GeTexamenesCargados obj)
        {
            _context.GeTexamenesCargados.UpdateRange(obj);
            await _context.SaveChangesAsync();
        }
    }
}
