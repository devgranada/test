﻿using ApiServicio.ContextoBD;
using ApiServicio.Entidades;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiServicio.Repositorio
{
    public class SedesRepository : ISedes
    {

        private readonly ApiContext _context;

        public SedesRepository(ApiContext context)
        {
            this._context = context;
        }

        public async ValueTask<List<GeTsedes>> GetAll()
        {
            var obj = await _context.GeTsedes.ToListAsync();
            return obj;
        }

        public async ValueTask<GeTsedes> GetId(int id)
        {
            var objeto = await _context.GeTsedes.SingleAsync(x => x.Id == id);
            return objeto;
        }

        public async ValueTask<GeTsedes> GetId(string sede)
        {
            var objeto = await _context.GeTsedes.SingleAsync(x => x.Nombre == sede);
            return objeto;
        }
    }
}
