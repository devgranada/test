﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Dto
{
    public class DTOExamProcesados
    {
        public string examen { get; set; }
        public string paciente { get; set; }
        public string fecha { get; set; }
        public string protocolo { get; set; }
        public string titulo { get; set; }
        public string examenA25 { get; set; }
        public string tipoMuestra { get; set; }
        public string resultado { get; set; }
        public string unidades { get; set; }
        public string fechaA25 { get; set; }
        public string horaA25 { get; set; }
    }
}
