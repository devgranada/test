﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Dto
{
    public class DTOInput
    {
        public string codigo { get; set; }
        public string mensaje { get; set; }
        public string estado { get; set; }
    }
}
