﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Dto
{
    public class DTOActExaLabFec
    {
        public string examen { get; set; }
        public string paciente { get; set; }
        public string fecha { get; set; }
    }
}
