﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Dto
{
    public class dtoExamenes
    {
        public string fecha { get; set; }
        public string sede { get; set; }
        public string paciente { get; set; }
    }
}
