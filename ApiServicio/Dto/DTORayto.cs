﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Dto
{
    public class DTORayto
    {
        public string examen { get; set; }
        public string paciente { get; set; }
        public string fecha { get; set; }
        public string hora { get; set; }
        public string protocolo { get; set; }
        public string titulo { get; set; }
        public string wbc { get; set; }
        public string lym1 { get; set; }
        public string mid1 { get; set; }
        public string gra1 { get; set; }
        public string lym2 { get; set; }
        public string mid2 { get; set; }
        public string gra2 { get; set; }
        public string rbc { get; set; }
        public string hgb { get; set; }
        public string mchc { get; set; }
        public string mch { get; set; }
        public string mcv { get; set; }
        public string rdw_cv { get; set; }
        public string rdw_sd { get; set; }
        public string hct { get; set; }
        public string plt { get; set; }
        public string mpv { get; set; }
        public string pdw { get; set; }
        public string pct { get; set; }
        public string p_lcr { get; set; }
        public string tubo { get; set; }
    }
}
