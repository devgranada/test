﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Dto
{
    public class DTOItemExamen
    {
        public string idExamen { get; set; }
        public string paciente { get; set; }
        public string texto { get; set; }

        public string fecha { get; set; }

        public string sede { get; set; }
    }
}
