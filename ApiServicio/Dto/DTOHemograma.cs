﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Dto
{
    public class DTOHemograma
    {
        public string examen { get; set; }
        public string paciente { get; set; }
        public string nombre { get; set; }
        public string fecha { get; set; }
        public string hora { get; set; }
        public string protocolo { get; set; }
        public string descripcion { get; set; }
        public string tubo { get; set; }
        public int carga { get; set; }
    }
}
