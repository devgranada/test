﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Dto
{
    public class DTOConexion
    {
        public string API_ClientName { get; set; }
        public string API_Key { get; set; }
        public string API_Secret { get; set; }
        public string API_RedirectURI { get; set; }
        public string API_SourceDomain { get; set; }
        public string API_SourceIP { get; set; }
        public string API_MasterUser { get; set; }
        public string API_MasterPassword { get; set; }
        public string API_AllowedFunctions { get; set; }
        public string URL { get; set; }
    }
}
