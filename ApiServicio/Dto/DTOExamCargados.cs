﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Dto
{
    public class DTOExamCargados
    {
        public string examen { get; set; }
        public string paciente { get; set; }
        public string tipo { get; set; }
        public string muestra { get; set; }
    }
}
