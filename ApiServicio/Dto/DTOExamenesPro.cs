﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Dto
{
    public class DTOExamenesPro
    {
        public string codigo { get; set; }
        public string examen { get; set; }
        public string fecha { get; set; }
        public string cedula { get; set; }
        public string nombre { get; set; }
        public string sede { get; set; }
        public int estado { get; set; }
    }
}
