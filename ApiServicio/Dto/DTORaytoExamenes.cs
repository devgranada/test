﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Dto
{
    public class DTORaytoExamenes
    {
        public string examen { get; set; }
        public string paciente { get; set; }
        public string fecha { get; set; }
        public string hora { get; set; }
        public string protocolo { get; set; }
        public string titulo { get; set; }
        public string codMuestra { get; set; }
        public string muestra { get; set; }
        public string valor { get; set; }
        public string tubo { get; set; }
    }
}
