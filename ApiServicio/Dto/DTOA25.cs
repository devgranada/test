﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Dto
{
    public class DTOA25
    {
        public string Paciente { get; set; }
        public string Id { get; set; }
        public string Fecha { get; set; }
        public string Tecnica { get; set; }
    }
}
