﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Dto
{
    public class DTOColesterolLDL
    {
        public string strFecha { get; set; }
        public string idExamen { get; set; }
        public string colesterol { get; set; }
        public string colesterolHDL { get; set; }
        public string colesterolVLDL { get; set; }
    }
}
