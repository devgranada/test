﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Dto
{
    public class DTOProcesadosA25
    {
        public string id { get; set; }
        public string examen { get; set; }
        public string protocolo { get; set; }
        public string tubo { get; set; }
        public string paciente { get; set; }
        public string nombre { get; set; }
        public string prueba { get; set; }
        public string resultado { get; set; }
        public string referencia { get; set; }
    }
}
