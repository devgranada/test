﻿using ApiServicio.Entidades;
using ApiServicio.Repositorio;
using ApiServicio.Servicio;
using Quartz;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ApiServicio.Tarea
{
    public class EjecutarTarea : IJob
    {
        private readonly IServicioProceso _IServicioProceso;
        private readonly IServicioProcesoA25 _IServicioProcesoA25;
        private readonly IParametrosRepository _IParametrosRepository;
        private readonly IA25ProcesarExamenes _IA25ProcesarExamenes;
        private readonly IRaytoProceso _IRaytoProceso;

        public EjecutarTarea(IServicioProceso _IServicioProceso,
                             IServicioProcesoA25 _IServicioProcesoA25,
                             IParametrosRepository _IParametrosRepository,
                             IA25ProcesarExamenes _IA25ProcesarExamenes,
                             IRaytoProceso _IRaytoProceso)
        {
            this._IServicioProceso = _IServicioProceso;
            this._IServicioProcesoA25 = _IServicioProcesoA25;
            this._IA25ProcesarExamenes = _IA25ProcesarExamenes;
            this._IParametrosRepository = _IParametrosRepository;
            this._IRaytoProceso = _IRaytoProceso;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                //await CrearPlano("Execute iniciar");
                List<GeTparametros> ListaParametros = await _IParametrosRepository.ConsultarParametros();

                var fechaProceso = ListaParametros.FirstOrDefault(a => a.Llave == "FECHA_PROCESO" && a.Estado == 1);

                var ParametroEjecucion = ListaParametros.FirstOrDefault(a => a.Llave == "EJECUCION_PROCESO" && a.Estado == 1);

                if (ParametroEjecucion.Valor.Equals("1"))
                    return;

                var TiempoEjecucion = ListaParametros.FirstOrDefault(a => a.Llave == "TIEMPO_AUTOMATICA" && a.Estado == 1);
                var UltimaHoraEjecucion = ListaParametros.FirstOrDefault(a => a.Llave == "ULTIMA_EJECUCION_AUTOMATICA" && a.Estado == 1);

                var ProximaEjecucion = Convert.ToDateTime(UltimaHoraEjecucion.Valor).AddMinutes(Convert.ToInt32(TiempoEjecucion.Valor));

                if (ProximaEjecucion > DateTime.Now)
                    return;

                await ActualizarEjecuciones(true, ListaParametros);

                // SOFIA
                string NombreCompletoPlano = string.Concat(DateTime.Now.ToString("dd-MMMM-yyyy_HH-mm-ss"), " Inicio "," Consultando Sofia");
                await CrearPlano(NombreCompletoPlano);
                await _IServicioProceso.EjecutarPendientes();
                NombreCompletoPlano = string.Concat(DateTime.Now.ToString("dd-MMMM-yyyy_HH-mm-ss"), " Fin ", " Consultando Sofia");
                await CrearPlano(NombreCompletoPlano);
                //A25
                var objA25 = ListaParametros.FirstOrDefault(a => a.Llave == "SERVICIO_A25" && a.Estado == 1);
                if (objA25.Valor.Equals("1"))
                {
                    NombreCompletoPlano = string.Concat(DateTime.Now.ToString("dd-MMMM-yyyy_HH-mm-ss"), " Inicio ", " Consultando A25");
                    await CrearPlano(NombreCompletoPlano);
                    await _IServicioProcesoA25.EjecutarA25();
                    NombreCompletoPlano = string.Concat(DateTime.Now.ToString("dd-MMMM-yyyy_HH-mm-ss"), " Fin ", " Consultando A25");
                    await CrearPlano(NombreCompletoPlano);
                    await _IA25ProcesarExamenes.Procesar(fechaProceso.Valor);
                    await _IA25ProcesarExamenes.UploadSofia();
                }
                // Rayto
                var objRayto = ListaParametros.FirstOrDefault(a => a.Llave == "SERVICIO_RAYTO" && a.Estado == 1);
                if (objRayto.Valor.Equals("1"))
                {
                    NombreCompletoPlano = string.Concat(DateTime.Now.ToString("dd-MMMM-yyyy_HH-mm-ss"), " Inicio ", " Consultando Rayto ");
                    await CrearPlano(NombreCompletoPlano);
                    NombreCompletoPlano = string.Concat(DateTime.Now.ToString("dd-MMMM-yyyy_HH-mm-ss"), " Fin ", " Consultando Rayto ");
                    await _IRaytoProceso.procesar(fechaProceso.Valor);
                    await CrearPlano(NombreCompletoPlano);
                    await _IRaytoProceso.cargarExamenes();
                }

                await ActualizarEjecuciones(false, ListaParametros);
            }
            catch (Exception e)
            {
                await CrearPlano(e.Message+" "+e.StackTrace);
            }

        }

        private async ValueTask ActualizarEjecuciones(bool IniciarEjecucion, List<GeTparametros> Parametros)
        {
            if (IniciarEjecucion)
            {

                var ParametroEjecucion = Parametros.FirstOrDefault(a => a.Llave == "EJECUCION_PROCESO" && a.Estado == 1);
                ParametroEjecucion.Valor = "1";

                List<GeTparametros> ListaActualizar = new List<GeTparametros>
                {
                    ParametroEjecucion
                };

                await _IParametrosRepository.ActualizarParametros(ListaActualizar);
            }
            else
            {
                var ParametroEjecucion = Parametros.FirstOrDefault(a => a.Llave == "EJECUCION_PROCESO" && a.Estado == 1);
                ParametroEjecucion.Valor = "0";

                var UltimaEjecucion = Parametros.FirstOrDefault(a => a.Llave == "ULTIMA_EJECUCION_AUTOMATICA" && a.Estado == 1);
                UltimaEjecucion.Valor = DateTime.Now.ToString();

                List<GeTparametros> ListaActualizar = new List<GeTparametros>()
                {
                    ParametroEjecucion,
                    UltimaEjecucion
                };

                await _IParametrosRepository.ActualizarParametros(ListaActualizar);
                var result = await _IParametrosRepository.SpFechaSistema();
            }
        }

        private async Task CrearPlano(string Datos)
        {
            GeTparametros rutaLog = await _IParametrosRepository.ConsultarParametroPorNombre("RUTALOG");
            if (rutaLog != null)
            {
                string Path = rutaLog.Valor; // @"G:\ErroresApi\";
                string NombreCompletoPlano = string.Concat(Path, "Log-", DateTime.Now.ToString("dd-MMMM-yyyy_HH"), ".txt");

                if (!Directory.Exists(Path))
                    Directory.CreateDirectory(Path);

                string[] linea = { Datos };
                await File.AppendAllLinesAsync(NombreCompletoPlano, linea);
            }

        }
    }
}
