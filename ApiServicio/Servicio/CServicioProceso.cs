﻿using ApiServicio.Dto;
using ApiServicio.Entidades;
using ApiServicio.Repositorio;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace ApiServicio.Servicio
{
    public class CServicioProceso : IServicioProceso
    {
        private readonly IExamenesRepository _IExamenesRepository;
        private readonly ISedes _ISedes;
        private readonly IParametrosRepository _IParametrosRepository;
        private readonly ICrearPlano _ICrearPlano;

        public CServicioProceso(IExamenesRepository _IExamenesRepository, 
                        ISedes _ISedes, 
                        IParametrosRepository _IParametrosRepository,
                        ICrearPlano _ICrearPlano)
        {
            this._IExamenesRepository = _IExamenesRepository;
            this._ISedes = _ISedes;
            this._IParametrosRepository = _IParametrosRepository;
            this._ICrearPlano = _ICrearPlano;
        }

        public async Task EjecutarPendientes()
        {

            List<GeTsedes> objSedes = await _ISedes.GetAll();
            foreach (GeTsedes item in objSedes)
            {
                await ProcesarPendientes(item);
            }

            /*if (!string.IsNullOrWhiteSpace(Pendientes))
                await CrearPlano(Pendientes);*/
        }

        private async Task CrearPlano(string Datos)
        {
            GeTparametros rutaLog = await _IParametrosRepository.ConsultarParametroPorNombre("RUTALOG");
            if (rutaLog != null)
            {
                string Path = rutaLog.Valor; // @"G:\ErroresApi\";
                string NombreCompletoPlano = string.Concat(Path, "Log-", DateTime.Now.ToString("dd-MMMM-yyyy_HH"), ".txt");

                if (!Directory.Exists(Path))
                    Directory.CreateDirectory(Path);

                string[] linea = { Datos };
                await File.AppendAllLinesAsync(NombreCompletoPlano, linea);
            }

        }

        private async ValueTask ProcesarPendientes(GeTsedes objSedes)
        {
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            String Xml;

            try
            {
                GeTparametros objParametros = await _IParametrosRepository.ConsultarParametroPorNombre("FECHA_PROCESO");
                DTOConexion cnx = new DTOConexion();
                cnx.API_Key = objSedes.ApiKey;
                cnx.API_Secret = objSedes.ApiSecret;
                cnx.API_RedirectURI = objSedes.ApiRedirectUri;

                //string url = "https://localhost:44304/weatherforecast";

                string url = cnx.API_RedirectURI + "?API_Key=" +
                             cnx.API_Key + "&API_Secret=" + cnx.API_Secret +
                             "&accion=ordenes_laboratorio_fecha&fecha_exploracion=" + objParametros.Valor;

                request = WebRequest.Create(url) as HttpWebRequest;
                using (response = request.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    Xml = reader.ReadToEnd();
                    if (Xml.Contains("ERR_04"))
                    {
                        //throw new Exception("Error al conectar con Sofia");
                        string mensaje = string.Concat(DateTime.Now.ToString("dd-MMMM-yyyy_HH-mm-ss"), " ERR_04 ", " Error al conectar con Sofia");
                        await _ICrearPlano.CrearPlano(mensaje);
                        return;
                    }

                }

                DataSet dts = new DataSet();
                System.IO.StringReader sr = new System.IO.StringReader(Xml);
                dts.ReadXml(sr);
                dts.AcceptChanges();
                if (dts.Tables.Count > 1)
                {
                    if (dts.Tables["examen"] != null & dts.Tables["examen"].Rows.Count > 0)
                    {
                        foreach (DataRow row in dts.Tables["examen"].Rows)
                        {
                            var objExam = await _IExamenesRepository.GetId(row["id"].ToString().Trim());
                            if (objExam == null)
                            {
                                GeTexamenes exam = new GeTexamenes();
                                exam = new GeTexamenes
                                {
                                    CodId = row["id"].ToString().Trim(),
                                    ProtocoloCodigo = row["protocolo_codigo"].ToString(),
                                    ProtocoloTitulo = row["protocolo_titulo"].ToString(),
                                    Tubo = row["tubo"].ToString(),
                                    TuboMuestra = row["tubo_muestra"].ToString(),
                                    Fecha = row["fecha"].ToString(),
                                    Hora = row["hora"].ToString(),
                                    Paciente = row["paciente"].ToString(),
                                    Nombre = row["nombre"].ToString(),
                                    Sexo = row["sexo"].ToString(),
                                    Edad = row["edad"].ToString(),
                                    FechaNacimiento = row["fecha_nacimiento"].ToString(),
                                    PacienteId = row["paciente_id"].ToString(),
                                    Sede = "Palmira"
                                };
                                if (exam.ProtocoloTitulo.Contains("HEMOGRAMA"))
                                {
                                    exam.Hemograma = true;
                                }

                                await _IExamenesRepository.guardar(exam);
                            }
                        }
                    }
                }
            }
            catch 
            {
                throw;
            }
        }
    }
}
