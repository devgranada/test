﻿using ApiServicio.Dto;
using ApiServicio.Entidades;
using ApiServicio.Repositorio;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ApiServicio.Servicio
{
    public class CA25ProcesarExamenes : IA25ProcesarExamenes
    {

        private readonly IExamenesRepository _IExamenesRepository;
        private readonly ISedes _ISedes;
        private readonly IParametrosRepository _IParametrosRepository;
        private readonly IA25Repository _Ia25Repository;
        private readonly IActExaLabFechaRepository _ActExaLabFechaRepository;
        private readonly IItemExamLabRepository _IItemExamLabRepository;
        private readonly IExamenesA25Repository _IExamenesA25Repository;
        private readonly IProcesosRepository _IProcesosRepository;
        private readonly ICargasRepository _ICargasRepository;
        private readonly IExamenesCargadosRepository _IExamenesCargadosRepository;

        public CA25ProcesarExamenes(IExamenesRepository _IExamenesRepository, 
            ISedes _ISedes, 
            IParametrosRepository _IParametrosRepository, 
            IA25Repository _Ia25Repository,
            IActExaLabFechaRepository _ActExaLabFechaRepository,
            IItemExamLabRepository _IItemExamLabRepository,
            IExamenesA25Repository _IExamenesA25Repository,
            IProcesosRepository _IProcesosRepository,
            ICargasRepository _ICargasRepository,
            IExamenesCargadosRepository _IExamenesCargadosRepository)
        {
            this._IExamenesRepository = _IExamenesRepository;
            this._ISedes = _ISedes;
            this._IParametrosRepository = _IParametrosRepository;
            this._Ia25Repository = _Ia25Repository;
            this._ActExaLabFechaRepository = _ActExaLabFechaRepository;
            this._IItemExamLabRepository = _IItemExamLabRepository;
            this._IExamenesA25Repository = _IExamenesA25Repository;
            this._IProcesosRepository = _IProcesosRepository;
            this._ICargasRepository = _ICargasRepository;
            this._IExamenesCargadosRepository = _IExamenesCargadosRepository;
        }

        public async ValueTask<List<DTOProcesadosA25>> Procesar(string InputFecha)
        {

            try
            {
                InputFecha = InputFecha.Substring(0, 4) + "-" + InputFecha.Substring(4, 2) + "-" + InputFecha.Substring(6, 2);
                List<GeTa25> ListaDatos = await _Ia25Repository.GetIdFecha(InputFecha);
                string strFecha = string.Empty;
                string strTecnica = string.Empty;
                string strPaciente = string.Empty;

                foreach (GeTa25 line in ListaDatos)
                {

                    /* Se realiza procesamiento de los examenes de sofia vs los resultados del dispositivo A25
                     * y se almacenan en la base de datos para procesarlos al sistema sofia
                     * por medio del web services 
                     * 
                     */
                    string strFechaA25 = InputFecha;
                    dtoExamenes inputExam = new dtoExamenes
                    {
                        fecha = strFechaA25,
                        paciente = "",
                        sede = ""
                    };
                    IList<DTOExamProcesados> lstExamSofiaA25 = await _IExamenesRepository.listExamProc(inputExam);

                    if (lstExamSofiaA25 == null)
                    {
                        throw new System.ArgumentException("No se ha descargado la información de los examenes de sofia al sistema,<p>para " +
                                                            " del dia " + strFecha);
                    }

                    decimal lnValorRes = 0;
                    decimal lnUrea = 2.14m;
                    foreach (DTOExamProcesados item in lstExamSofiaA25)
                    {
                        ActExamenlabFecha examCab = new ActExamenlabFecha();
                        examCab = await _ActExaLabFechaRepository.ById(item.examen);
                        if (examCab == null)
                        {
                            examCab = new ActExamenlabFecha();
                            examCab.IdExamen = item.examen;
                            examCab.Paciente = item.paciente;
                            examCab.Fecha = item.fecha;
                            examCab.ResultadoGlobal = "Normal"; // validar con el valor de referencia
                            examCab.Responsable = "PENDIENTEVALIDAR";
                            examCab.Nota = "";
                            examCab.Cargado = "N";

                            await _ActExaLabFechaRepository.guardar(examCab);
                        }

                        DTOActExaLabFec objExaLab = new DTOActExaLabFec
                        {
                            examen = examCab.IdExamen,
                            paciente = examCab.Paciente,
                            fecha = examCab.Fecha
                        };
                        ActExamenlabFecha idCab = await _ActExaLabFechaRepository.IdCabecera(objExaLab);

                        ItemExamenlab examDetalle = new ItemExamenlab();
                        DTOItemExamen objItemExa = new DTOItemExamen
                        {
                            idExamen = item.examen,
                            paciente = item.paciente,
                            texto = item.titulo
                        };

                        examDetalle = await _IItemExamLabRepository.GetByExamen(objItemExa);

                        if (examDetalle == null)
                        {
                            examDetalle = new ItemExamenlab();
                            examDetalle.IdCabecera = idCab.Id;
                            examDetalle.IdExamen = item.examen;
                            examDetalle.Paciente = item.paciente;
                            examDetalle.Texto = item.titulo;
                            examDetalle.ValorReferencia = "";
                            examDetalle.ValorAdicional = "";

                            if (item.examenA25.Trim().Equals("NITROGENO UREICO [BUN]"))
                            {
                                lnValorRes = decimal.Parse(item.resultado) / lnUrea;
                                lnValorRes = Math.Round(lnValorRes, 1);
                                examDetalle.ValorCualitativo = lnValorRes.ToString();
                                examDetalle.ValorReferencia = " 7.0 A 18.0 mg/dl";
                            }
                            else
                            {
                                /* cualquier otro examen diferente a la UREA Y LOS TRIGLICERIDOS*/
                                examDetalle.ValorCualitativo = item.resultado;
                                if (item.examenA25.Trim().Equals("GLUCOSE"))
                                {
                                    if (decimal.Parse(item.resultado) < 70 || decimal.Parse(item.resultado) > 105)
                                    {
                                        examDetalle.ValorAdicional = "*";
                                    }
                                }

                                GeTexamenesA25 exA25 = new GeTexamenesA25();
                                DTOInput objExA25 = new DTOInput
                                {
                                    codigo = item.examenA25
                                };

                                exA25 = await _IExamenesA25Repository.Examenes(objExA25);
                                if (exA25 != null)
                                {
                                    examDetalle.ValorReferencia = exA25.Descripcion;
                                    if (item.examenA25.Trim().Equals("ALT"))
                                        examDetalle.ValorAdicional = decimal.Parse(item.resultado) > 42 ? "*" : "";

                                    if (item.examenA25.Trim().Equals("AST"))
                                    {
                                        examDetalle.ValorAdicional = decimal.Parse(item.resultado) > 41 ? "*" : "";
                                    }
                                }
                            }
                            examDetalle.Fecha = item.fecha;

                            if (item.examenA25.Trim().Equals("CHOLINESTERASE"))
                            {
                                GeTexamenes examenA25 = new GeTexamenes();
                                examenA25 = await _IExamenesRepository.GetId(item.examen);
                                if (examenA25 != null)
                                {
                                    if (examenA25.Sexo.Equals("M")) examDetalle.ValorReferencia = "Hombres 4620 - 11500 U/L";

                                    if (examenA25.Sexo.Equals("F")) examDetalle.ValorReferencia = "Mujeres 3930 - 10800 U/L";
                                }
                            }

                            // acido urico - URIC ACID
                            if (item.examenA25.Trim().Equals("URIC ACID"))
                            {
                                GeTexamenes examenA25 = new GeTexamenes();
                                examenA25 = await _IExamenesRepository.GetId(item.examen);
                                if (examenA25 != null)
                                {
                                    if (examenA25.Sexo.Equals("M"))
                                    {
                                        examDetalle.ValorReferencia = "Hombres 3.5 - 7.2 mg/dL";
                                        String vl = "7.2";
                                        examDetalle.ValorAdicional = decimal.Parse(item.resultado) > decimal.Parse(vl) ? "*" : "";
                                    }

                                    if (examenA25.Sexo.Equals("F"))
                                    {
                                        examDetalle.ValorReferencia = "Mujeres 2.6 - 6 mg/dL";
                                        String vl = "6";
                                        examDetalle.ValorAdicional = decimal.Parse(item.resultado) > decimal.Parse(vl) ? "*" : "";
                                    }
                                }
                            }



                            if (item.examenA25.Trim().Equals("TRIGLYCERIDES"))
                            {
                                if (decimal.Parse(item.resultado) > 0 & decimal.Parse(item.resultado) <= 150)
                                {
                                    examDetalle.ValorReferencia = "Normal : 150 mg/dL";
                                }

                                if (decimal.Parse(item.resultado) > 150 & decimal.Parse(item.resultado) < 401)
                                {
                                    examDetalle.ValorReferencia = "Alto: 150-400 mg/dL";
                                    examDetalle.ValorAdicional = "*";
                                }

                                if (decimal.Parse(item.resultado) > 400)
                                {
                                    examDetalle.ValorReferencia = "Muy Alto: >400 mg/dL";
                                    examDetalle.ValorAdicional = "*";
                                }
                            }

                            //if (item.examenA25.Trim().Equals("COLESTEROL TOTAL"))
                            if (item.examenA25.Trim().Equals("CHOLESTEROL"))
                            {
                                if (decimal.Parse(item.resultado) <= 199)
                                    examDetalle.ValorReferencia = "ideal: <200 mg/dL";

                                if (decimal.Parse(item.resultado) > 199 & decimal.Parse(item.resultado) < 241)
                                {
                                    examDetalle.ValorReferencia = "Límite: 200-240 mg/dL";
                                    examDetalle.ValorAdicional = "*";
                                }

                                if (decimal.Parse(item.resultado) > 240)
                                {
                                    examDetalle.ValorReferencia = "Alto: >240 mg/dL";
                                    examDetalle.ValorAdicional = "*";
                                }
                            }

                            //examDetalle.valor_adicional = ""; // valor opcional
                            await _IItemExamLabRepository.adicionar(examDetalle);
                        }
                    }

                    //lbMensaje.Text = "Informacion cargada con Exito";
                }

                return await PerfilLipidico(InputFecha);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        private async ValueTask<List<DTOProcesadosA25>> PerfilLipidico(string fecha)
        {
            try
            {
                string strFechaA25 = fecha;
                decimal lnvldl = 0;
                decimal lnvTrigicerios = 0;

                dtoExamenes inputExam = new dtoExamenes
                {
                    fecha = strFechaA25,
                    paciente = "",
                    sede = ""
                };

                IList<DTOExamProcesados> lstExamSofiaA25 = await _IExamenesRepository.listExamPerfil(inputExam);

                foreach (DTOExamProcesados item in lstExamSofiaA25)
                {

                    ActExamenlabFecha examCab = await _ActExaLabFechaRepository.ById(item.examen);
                    if (examCab == null)
                    {
                        examCab = new ActExamenlabFecha();
                        examCab.IdExamen = item.examen;
                        examCab.Paciente = item.paciente;
                        examCab.Fecha = item.fecha;
                        examCab.ResultadoGlobal = "Normal"; // validar con el valor de referencia
                        examCab.Responsable = "PENDIENTEVALIDAR";
                        examCab.Nota = "";
                        examCab.Cargado = "N";
                        await _ActExaLabFechaRepository.guardar(examCab);
                    }

                    DTOActExaLabFec objExaLab = new DTOActExaLabFec
                    {
                        examen = examCab.IdExamen,
                        paciente = examCab.Paciente,
                        fecha = examCab.Fecha
                    };
                    ActExamenlabFecha idCab = await _ActExaLabFechaRepository.IdCabecera(objExaLab);

                    ItemExamenlab examDetalle = new ItemExamenlab();

                    DTOItemExamen objItemExa = new DTOItemExamen
                    {
                        idExamen = item.examen,
                        paciente = item.paciente,
                        texto = item.titulo
                    };

                    examDetalle = await _IItemExamLabRepository.GetByExamen(objItemExa);

                    //examDetalle = detExamen.GetByExamen(item.examen, item.paciente, item.titulo.Trim());
                    if (examDetalle == null)
                    {
                        examDetalle = new ItemExamenlab();
                        examDetalle.IdCabecera = idCab.Id;
                        examDetalle.IdExamen = item.examen;
                        examDetalle.Paciente = item.paciente;
                        examDetalle.Texto = item.titulo;
                        examDetalle.ValorCualitativo = item.resultado;
                        examDetalle.Fecha = item.fecha;
                        examDetalle.ValorAdicional = "";
                        switch (item.titulo.Trim())
                        {
                            case "COLESTEROL TOTAL":
                                if (decimal.Parse(item.resultado) < 200)
                                    examDetalle.ValorReferencia = "ideal: <200 mg/dL";

                                if (decimal.Parse(item.resultado) > 199 & decimal.Parse(item.resultado) < 241)
                                {
                                    examDetalle.ValorReferencia = "Límite: 200-240 mg/dL";
                                    examDetalle.ValorAdicional = "*";
                                }

                                if (decimal.Parse(item.resultado) > 240)
                                {
                                    examDetalle.ValorReferencia = "Alto: >240 mg/dL";
                                    examDetalle.ValorAdicional = "*";
                                }
                                break;

                            case "TRIGLICERIDOS":
                                if (decimal.Parse(item.resultado) > 0 & decimal.Parse(item.resultado) < 149)
                                    examDetalle.ValorReferencia = "Normal : 149 mg/dL";

                                if (decimal.Parse(item.resultado) > 149 & decimal.Parse(item.resultado) < 401)
                                {
                                    examDetalle.ValorReferencia = "Alto: 150-400 mg/dL";
                                    examDetalle.ValorAdicional = "*";
                                }

                                if (decimal.Parse(item.resultado) > 400)
                                {
                                    examDetalle.ValorReferencia = "Muy Alto: >400 mg/dL";
                                    examDetalle.ValorAdicional = "*";
                                }
                                break;

                            case "COLESTEROL DE ALTA DENSIDAD (HDL)":
                                GeTexamenes exem = new GeTexamenes();
                                exem = await _IExamenesRepository.GetId(item.examen);
                                //exem = examenesSofia.getbyId(item.examen);
                                if (exem != null)
                                {
                                    if (exem.Sexo.Equals("M")) examDetalle.ValorReferencia = "35-120 mg/Dl";

                                    if (exem.Sexo.Equals("F")) examDetalle.ValorReferencia = "45-120 mg/dL";
                                }
                                break;

                            default:
                                examDetalle.ValorReferencia = "";
                                break;
                        }
                        //examDetalle.valor_adicional = ""; // valor opcional
                        //detExamen.Add(examDetalle);
                        await _IItemExamLabRepository.adicionar(examDetalle);
                    }

                    if (item.titulo.Trim().Equals("TRIGLICERIDOS"))
                    {
                        lnvTrigicerios = item.resultado.Contains('*') ? decimal.Parse(item.resultado.Substring(0, item.resultado.Length - 1)) : decimal.Parse(item.resultado);

                        objItemExa.idExamen = item.examen;
                        objItemExa.paciente = item.paciente;
                        objItemExa.texto = "COLESTEROL DE MUY BAJA DENSIDAD (VLDL)";
                        examDetalle = await _IItemExamLabRepository.GetByExamen(objItemExa);
                        //examDetalle = detExamen.GetByExamen(item.examen, item.paciente, "COLESTEROL DE MUY BAJA DENSIDAD (VLDL)");
                        if (examDetalle == null)
                        {
                            lnvldl = decimal.Parse(item.resultado) / 5;
                            examDetalle = new ItemExamenlab();
                            examDetalle.IdCabecera = idCab.Id;
                            examDetalle.IdExamen = item.examen;
                            examDetalle.Paciente = item.paciente;
                            examDetalle.Texto = "COLESTEROL DE MUY BAJA DENSIDAD (VLDL)";
                            examDetalle.ValorAdicional = "";

                            if (lnvTrigicerios < 400)
                            {
                                examDetalle.ValorCualitativo = lnvldl.ToString();
                            }
                            else
                            {
                                examDetalle.ValorCualitativo = lnvldl.ToString();
                                examDetalle.ValorAdicional = "*";
                            }
                            examDetalle.Fecha = item.fecha;
                            examDetalle.ValorReferencia = "Menor de 30 mg/dL"; // se debe de colocar el valor de referencia de los examenes;
                            //examDetalle.valor_adicional = ""; // valor opcional
                            //detExamen.Add(examDetalle);
                            await _IItemExamLabRepository.adicionar(examDetalle);
                        }
                    }

                    DTOColesterolLDL objLDL = new DTOColesterolLDL
                    {
                        strFecha = item.fecha,
                        idExamen = item.examen,
                        colesterol = "COLESTEROL TOTAL",
                        colesterolHDL = "COLESTEROL DE ALTA DENSIDAD (HDL)",
                        colesterolVLDL = "COLESTEROL DE MUY BAJA DENSIDAD (VLDL)"
                    };

                    decimal lnLDL = await _IItemExamLabRepository.lstColesterolLDL(objLDL);
                    //decimal lnLDL = detExamen.lstColesterolLDL(item.fecha, item.examen, "COLESTEROL TOTAL", "COLESTEROL DE ALTA DENSIDAD (HDL)", "COLESTEROL DE MUY BAJA DENSIDAD (VLDL)");

                    if (lnLDL != 0)
                    {
                        ItemExamenlab examDetalle1 = new ItemExamenlab();
                        objItemExa.idExamen = item.examen;
                        objItemExa.paciente = item.paciente;
                        objItemExa.texto = "TRIGLICERIDOS";
                        examDetalle1 = await _IItemExamLabRepository.GetByExamen(objItemExa);
                        //examDetalle1 = detExamen.GetByExamen(item.examen, item.paciente, "TRIGLICERIDOS");

                        lnvTrigicerios = examDetalle1.ValorCualitativo.Contains('*') ? decimal.Parse(examDetalle1.ValorCualitativo.Substring(0, examDetalle1.ValorCualitativo.Length - 1)) : decimal.Parse(examDetalle1.ValorCualitativo);
                        objItemExa.idExamen = item.examen;
                        objItemExa.paciente = item.paciente;
                        objItemExa.texto = "COLESTEROL DE BAJA DENSIDAD (LDL)";
                        //examDetalle = detExamen.GetByExamen(item.examen, item.paciente, "COLESTEROL DE BAJA DENSIDAD (LDL)");
                        examDetalle = await _IItemExamLabRepository.GetByExamen(objItemExa);
                        if (examDetalle == null)
                        {
                            examDetalle = new ItemExamenlab();
                            examDetalle.IdCabecera = idCab.Id;
                            examDetalle.IdExamen = item.examen;
                            examDetalle.Paciente = item.paciente;
                            examDetalle.Texto = "COLESTEROL DE BAJA DENSIDAD (LDL)";
                            examDetalle.ValorAdicional = "";
                            if (lnvTrigicerios < 400)
                            {
                                examDetalle.ValorCualitativo = lnLDL.ToString();
                            }
                            else
                            {
                                examDetalle.ValorCualitativo = lnLDL.ToString();
                                examDetalle.ValorAdicional = "*";
                            }
                            examDetalle.Fecha = item.fecha;
                            examDetalle.ValorReferencia = ""; // se debe de colocar el valor de referencia de los examenes;

                            if (lnLDL < 100) examDetalle.ValorReferencia = "Rango cercano a lo optimo: <100 mg/dL";

                            if (lnLDL > 129 & lnLDL < 160) examDetalle.ValorReferencia = "Limite normal alto: 130-159 mg/dL";

                            if (lnLDL > 159 & lnLDL < 190) examDetalle.ValorReferencia = "Valor elevado: 160-189 mg/dL";

                            if (lnLDL > 190) examDetalle.ValorReferencia = "Limite muy alto: >190 mg/dL";

                            //examDetalle.valor_adicional = ""; // valor opcional
                            //detExamen.Add(examDetalle);
                            await _IItemExamLabRepository.adicionar(examDetalle);
                        }
                    }
                }

                DTOItemExamen objItemExaConsulta = new DTOItemExamen
                {
                    fecha = fecha,
                    sede = ""
                };

                return await _IItemExamLabRepository.lstProcesadosA25(objItemExaConsulta);
                //lbMensaje.Text = "Perfiles procesados con Exito";

            }
            catch
            {
                //lbMensaje.Text = "Se presento un error cargando la infomación";
                throw;
            }
        }

        public async ValueTask<int> cargarExamenes(string fecha, string sede)
        {
            try
            {
                int idProceso = 0;
                DTOItemExamen objItemExa = new DTOItemExamen
                {
                    fecha = fecha,
                    sede = sede
                };

                List<DTOExamenesPro> lstExamenes = await _IItemExamLabRepository.lstCarga(objItemExa);

                if (lstExamenes != null && lstExamenes.Count > 0)
                {

                    var objSedes = await _ISedes.GetId(sede);
                    DTOConexion cnx = new DTOConexion();
                    cnx.API_Key = objSedes.ApiKey;
                    cnx.API_Secret = objSedes.ApiSecret;
                    cnx.API_RedirectURI = objSedes.ApiRedirectUri;

                    string url = cnx.API_RedirectURI + "?API_Key=" +
                                 cnx.API_Key + "&API_Secret=" + cnx.API_Secret +
                                 "&accion=actualizar_examenlab_fecha";

                    IList<ActExamenlabFecha> lstCab = new List<ActExamenlabFecha>();
                    lstCab = await _ActExaLabFechaRepository.Get(fecha, sede);
                    string strEncabezado;
                    bool op = false;

                    GeTprocesos objProc = new GeTprocesos
                    {
                        Nombre = "SUBIRA25",
                        Descripcion = "Se suben los resultados de laboratorio a Sofia",
                        Fecha = DateTime.Now,
                        Sede = sede,
                        Cargados = "N"
                    };

                    await _IProcesosRepository.guardar(objProc);

                    idProceso = objProc.Id;

                    foreach (ActExamenlabFecha item in lstCab)
                    {
                        DTOExamCargados objBuscarExam = new DTOExamCargados
                        {
                            examen = item.IdExamen,
                            paciente = item.Paciente,
                            tipo = "C",
                            muestra = item.IdExamen
                        };
                        op = await _IExamenesCargadosRepository.buscarExam(objBuscarExam);
                        if (!op)
                        {
                            strEncabezado = "&idexamen=" + item.IdExamen +
                                            "&paciente=" + item.Paciente +
                                            "&fecha=" + item.Fecha +
                                            "&resultado_global=" + item.ResultadoGlobal +
                                            "&responsable=" + item.Responsable +
                                            "&notas=" + item.Nota;
                            GeTcargas objCarga = new GeTcargas();
                            objCarga.Cadena = url + strEncabezado;
                            objCarga.Fecha = DateTime.Today.ToString("yyyyMMdd");
                            objCarga.Hora = DateTime.Now.ToString("t");
                            objCarga.IdProceso = objProc.Id;
                            objCarga.IdExamen = int.Parse(item.IdExamen);
                            objCarga.IdPaciente = item.Paciente;
                            //objCarga.respuesta = wsSofia(objCarga.Cadena);
                            //lblMensaje.Text = "Cargado con Exito el examen" + objCarga.respuesta;
                            await _ICargasRepository.guardar(objCarga);
                            await _IExamenesCargadosRepository.guardar(new GeTexamenesCargados
                            {
                                IdExamenSofia = item.IdExamen,
                                IdPaciente = item.Paciente,
                                Muestra = item.IdExamen,
                                Tipo = "C",
                                IdProceso = objProc.Id,
                                IdCarga = objCarga.Id
                            });

                            // cargado se coloca en P => PENDIENTE DE CARGA
                            item.Cargado = "P";
                            item.Proceso = idProceso;
                            item.FechaProceso = DateTime.Now;
                            await _ActExaLabFechaRepository.actualizar(item);
                        }
                    }

                    IList<ItemExamenlab> lstDetalle = await _IItemExamLabRepository.lstExamen(objItemExa);

                    string strDetalle;
                    string strResultadoFinal;
                    url = cnx.API_RedirectURI + "?API_Key=" +
                                 cnx.API_Key + "&API_Secret=" + cnx.API_Secret +
                                 "&accion=agregar_item_examenlab";
                    foreach (ItemExamenlab item in lstDetalle)
                    {
                        DTOExamCargados objBuscarExam = new DTOExamCargados
                        {
                            examen = item.IdExamen,
                            paciente = item.Paciente,
                            tipo = "D",
                            muestra = item.IdExamen
                        };
                        op = await _IExamenesCargadosRepository.buscarExam(objBuscarExam);

                        if (!op)
                        {
                            if (string.IsNullOrEmpty(item.ValorAdicional))
                            {
                                strResultadoFinal = item.ValorCualitativo.Trim();
                            }
                            else
                            {
                                strResultadoFinal = item.ValorCualitativo.Trim() + item.ValorAdicional.Trim();
                            }

                            strDetalle = "&idexamen=" + item.IdExamen +
                                            "&paciente=" + item.Paciente +
                                            "&fecha=" + item.Fecha +
                                            "&texto=" + item.Texto +
                                            "&valor_cualitativo=" + strResultadoFinal +
                                            "&valor_referencia=" + item.ValorReferencia +
                                            "&valor_adicional=" + "";
                            GeTcargas objCarga = new GeTcargas();
                            objCarga.Cadena = url + strDetalle;
                            objCarga.Fecha = DateTime.Today.ToString("yyyyMMdd");
                            objCarga.Hora = DateTime.Now.ToString("t");
                            objCarga.IdProceso = objProc.Id;
                            objCarga.IdExamen = int.Parse(item.IdExamen);
                            objCarga.IdPaciente = item.Paciente;
                            //objCarga.respuesta = wsSofia(objCarga.Cadena);
                            //lblMensaje.Text = "Cargado con Exito el examen" + objCarga.respuesta;
                            //Show("Cargado con Exito el examen" + objCarga.respuesta);
                            await _ICargasRepository.guardar(objCarga);
                            await _IExamenesCargadosRepository.guardar(new GeTexamenesCargados
                            {
                                IdExamenSofia = item.IdExamen,
                                IdPaciente = item.Paciente,
                                Muestra = item.Texto,
                                Tipo = "D",
                                IdProceso = objProc.Id,
                                IdCarga = objCarga.Id
                            });
                        }
                    }

                    //var objProceso = await UploadSofia(idProceso);

                }


                return idProceso;

            }
            catch
            {
                throw;
            }
        }

        public async ValueTask UploadSofia()
        {
            try
            {
                List<GeTprocesos> lstProcesosPdte = await _IProcesosRepository.GetEstadoN();

                foreach (GeTprocesos t in lstProcesosPdte) {

                    List<GeTcargas> lstProcesos = await _ICargasRepository.cargas(t.Id);
                    foreach (GeTcargas item in lstProcesos)
                    {
                        item.Respuesta = wsSofia(item.Cadena);
                        await _ICargasRepository.actualizar(item);
                        DTOActExaLabFec obj = new DTOActExaLabFec
                        {
                            examen = item.IdExamen.ToString(),
                            paciente = item.IdPaciente
                        };
                        ActExamenlabFecha objActExam = await _ActExaLabFechaRepository.GetExamenPaciente(obj);
                        objActExam.Cargado = item.Respuesta;
                        objActExam.FechaCarga = DateTime.Today.ToString("yyyyMMdd");
                        await _ActExaLabFechaRepository.actualizar(objActExam);
                    }

                    var objProceso = await _IProcesosRepository.GetId(t.Id);
                    objProceso.Cargados = "S";
                    await _IProcesosRepository.actualizar(objProceso);
                }
            }
            catch
            {
                throw;
            }
        }

        private string wsSofia(string url)
        {
            try
            {
                HttpWebRequest request = null;
                HttpWebResponse response = null;
                String Xml;
                request = WebRequest.Create(url) as HttpWebRequest;
                using (response = request.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    Xml = reader.ReadToEnd();
                    if (Xml.Contains("ERR_04"))
                    {
                        return "N";
                    }
                }

                DataSet dts = new DataSet();
                System.IO.StringReader sr = new System.IO.StringReader(Xml);
                dts.ReadXml(sr);
                dts.AcceptChanges();
                string respuesta = "";
                foreach (DataRow row in dts.Tables["resultado_ws"].Rows)
                {
                    respuesta = row["respuesta"].ToString().Trim();
                    break;
                }

                return respuesta;
            }
            catch
            {
                throw;
            }
        }

    }
}
