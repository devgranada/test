﻿using ApiServicio.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Servicio
{
    public interface IA25ProcesarExamenes
    {
        ValueTask<List<DTOProcesadosA25>> Procesar(string InputFecha);
        ValueTask UploadSofia();
    }
}
