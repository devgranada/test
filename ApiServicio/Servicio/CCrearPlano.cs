﻿using ApiServicio.Entidades;
using ApiServicio.Repositorio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Servicio
{
    public class CCrearPlano : ICrearPlano
    {
        private readonly IParametrosRepository _IParametrosRepository;

        public CCrearPlano(IParametrosRepository _IParametrosRepository)
        {
            this._IParametrosRepository = _IParametrosRepository;
        }

        public async Task CrearPlano(string Datos)
        {
            GeTparametros rutaLog = await _IParametrosRepository.ConsultarParametroPorNombre("RUTALOG");
            if (rutaLog != null)
            {
                string Path = rutaLog.Valor; // @"G:\ErroresApi\";
                string NombreCompletoPlano = string.Concat(Path, "Log-", DateTime.Now.ToString("dd-MMMM-yyyy_HH"), ".txt");

                if (!Directory.Exists(Path))
                    Directory.CreateDirectory(Path);

                string[] linea = { Datos };
                await File.AppendAllLinesAsync(NombreCompletoPlano, linea);
            }

        }
    }
}
