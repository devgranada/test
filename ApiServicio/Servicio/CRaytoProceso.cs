﻿using ApiServicio.Dto;
using ApiServicio.Entidades;
using ApiServicio.Repositorio;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ApiServicio.Servicio
{
    public class CRaytoProceso : IRaytoProceso
    {

        private readonly IExamenesRepository _IExamenesRepository;
        private readonly ISedes _ISedes;
        private readonly IRaytoRepositorio _IRaytoRepositorio;
        private readonly IActExaLabFechaRepository _ActExaLabFechaRepository;
        private readonly IItemExamLabRepository _IItemExamLabRepository;
        private readonly IExamenesA25Repository _IExamenesA25Repository;
        private readonly IProcesosRepository _IProcesosRepository;
        private readonly ICargasRepository _ICargasRepository;

        public CRaytoProceso(IExamenesRepository _IExamenesRepository,
            ISedes _ISedes,
            IRaytoRepositorio _IRaytoRepositorio,
            IActExaLabFechaRepository _ActExaLabFechaRepository,
            IItemExamLabRepository _IItemExamLabRepository,
            IExamenesA25Repository _IExamenesA25Repository,
            IProcesosRepository _IProcesosRepository,
            ICargasRepository _ICargasRepository)
        {
            this._IExamenesRepository = _IExamenesRepository;
            this._ISedes = _ISedes;
            this._IRaytoRepositorio = _IRaytoRepositorio;
            this._ActExaLabFechaRepository = _ActExaLabFechaRepository;
            this._IItemExamLabRepository = _IItemExamLabRepository;
            this._IExamenesA25Repository = _IExamenesA25Repository;
            this._IProcesosRepository = _IProcesosRepository;
            this._ICargasRepository = _ICargasRepository;
        }

        public async Task procesar(string InputFecha)
        {
            try
            {
                InputFecha = InputFecha.Substring(0, 4) + "-" + InputFecha.Substring(4, 2) + "-" + InputFecha.Substring(6, 2);
                List<DTORaytoExamenes> lstExamRayto = await _IRaytoRepositorio.listExamRayto(InputFecha);
                bool op = false;
                ActExamenlabFecha examCab = new ActExamenlabFecha();
                foreach (DTORaytoExamenes item in lstExamRayto)
                {
                    examCab = await _ActExaLabFechaRepository.ById(item.examen);
                    if (examCab == null)
                    {
                        examCab = new ActExamenlabFecha();
                        examCab.IdExamen = item.examen;
                        examCab.Paciente = item.paciente;
                        examCab.Fecha = item.fecha;
                        examCab.ResultadoGlobal = "Normal"; // validar con el valor de referencia
                        examCab.Responsable = "PENDIENTEVALIDAR";
                        examCab.Nota = "";
                        examCab.Cargado = "N";
                        await _ActExaLabFechaRepository.guardar(examCab);

                        op = await _IRaytoRepositorio.GetPaciente(item.tubo.Trim(), item.fecha.Trim());
                    }

                    DTOActExaLabFec obj = new DTOActExaLabFec
                    {
                        fecha = examCab.Fecha,
                        paciente = examCab.Paciente,
                        examen = examCab.IdExamen
                    };

                    ActExamenlabFecha idCab = await _ActExaLabFechaRepository.IdCabecera(obj);

                    string strResul = item.valor.Replace('.', ',');
                    float resultado = float.Parse(strResul);
                    ItemExamenlab examDetalle = new ItemExamenlab();

                    DTOItemExamen objExam = new DTOItemExamen
                    {
                        idExamen = item.examen,
                        paciente = item.paciente,
                        texto = item.muestra
                    };

                    examDetalle = await _IItemExamLabRepository.GetByExamen(objExam);
                    if (examDetalle == null)
                    {
                        examDetalle = new ItemExamenlab();
                        examDetalle.IdCabecera = idCab.Id;
                        examDetalle.IdExamen = item.examen;
                        examDetalle.Paciente = item.paciente;
                        GeTexamenesA25 exA25 = new GeTexamenesA25();
                        DTOInput objA25 = new DTOInput
                        {
                            codigo = item.muestra
                        };
                        exA25 = await _IExamenesA25Repository.Examenes(objA25);
                        examDetalle.Texto = item.muestra;
                        examDetalle.ValorCualitativo = item.valor;
                        examDetalle.Fecha = item.fecha;
                        examDetalle.ValorReferencia = exA25.Descripcion;

                        if (item.muestra.Equals("WBC"))
                        {
                            if (resultado < 4.50 || resultado > 10.20)
                                examDetalle.ValorCualitativo = examDetalle.ValorCualitativo + "*";
                        }

                        if (item.muestra.Equals("LYM%"))
                        {
                            if (resultado < 20.00 || resultado > 50.00)
                                examDetalle.ValorCualitativo = examDetalle.ValorCualitativo + "*";
                        }

                        if (item.muestra.Equals("MID%"))
                        {
                            if (resultado < 1.00 || resultado > 15.00)
                                examDetalle.ValorCualitativo = examDetalle.ValorCualitativo + "*";
                        }

                        if (item.muestra.Equals("GRA%"))
                        {
                            if (resultado < 40.00 || resultado > 70.00)
                                examDetalle.ValorCualitativo = examDetalle.ValorCualitativo + "*";
                        }

                        if (item.muestra.Equals("HGB"))
                        {
                            if (resultado < 12.00 || resultado > 18.00)
                                examDetalle.ValorCualitativo = examDetalle.ValorCualitativo + "*";
                        }

                        if (item.muestra.Equals("HCT"))
                        {
                            if (resultado < 36.00 || resultado > 54.00)
                                examDetalle.ValorCualitativo = examDetalle.ValorCualitativo + "*";
                        }

                        if (item.muestra.Equals("PLT"))
                        {
                            if (resultado < 150.00 || resultado > 450.00)
                                examDetalle.ValorCualitativo = examDetalle.ValorCualitativo + "*";
                        }

                        await _IItemExamLabRepository.adicionar(examDetalle);
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public async ValueTask cargarExamenes()
        {
            try
            {
                List<GeTprocesos> lstProcesosPdte = await _IProcesosRepository.GetEstadoN();
                foreach (GeTprocesos t in lstProcesosPdte)
                {
                    List<GeTcargas> lstProcesos = await _ICargasRepository.cargas(t.Id);
                    foreach (GeTcargas item in lstProcesos)
                    {
                        item.Respuesta = wsSofia(item.Cadena);
                        await _ICargasRepository.actualizar(item);
                        DTOActExaLabFec obj = new DTOActExaLabFec
                        {
                            examen = item.IdExamen.ToString(),
                            paciente = item.IdPaciente
                        };
                        ActExamenlabFecha objActExam = await _ActExaLabFechaRepository.GetExamenPaciente(obj);
                        objActExam.Cargado = item.Respuesta;
                        objActExam.FechaCarga = DateTime.Today.ToString("yyyyMMdd");
                        await _ActExaLabFechaRepository.actualizar(objActExam);
                    }
                    var objProceso = await _IProcesosRepository.GetId(t.Id);
                    objProceso.Cargados = "S";
                    await _IProcesosRepository.actualizar(objProceso);
                }

            }
            catch
            {
                throw;
            }
        }

        private string wsSofia(string url)
        {
            try
            {
                HttpWebRequest request = null;
                HttpWebResponse response = null;
                String Xml;
                request = WebRequest.Create(url) as HttpWebRequest;
                using (response = request.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    Xml = reader.ReadToEnd();
                    if (Xml.Contains("ERR_04"))
                    {
                        return "N";
                    }
                }

                DataSet dts = new DataSet();
                System.IO.StringReader sr = new System.IO.StringReader(Xml);
                dts.ReadXml(sr);
                dts.AcceptChanges();
                string respuesta = "";
                foreach (DataRow row in dts.Tables["resultado_ws"].Rows)
                {
                    respuesta = row["respuesta"].ToString().Trim();
                    break;
                }

                return respuesta;
            }
            catch
            {
                throw;
            }
        }
    }
}
