﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServicio.Servicio
{
    public interface IServicioProceso
    {
        Task EjecutarPendientes();
    }
}
