﻿using ApiServicio.Entidades;
using ApiServicio.Repositorio;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace ApiServicio.Servicio
{
    public class CServicioProcesoA25 : IServicioProcesoA25
    {
        private readonly IA25Repository _IA25Repository;
        private readonly IParametrosRepository _IParametrosRepository;

        public CServicioProcesoA25(IA25Repository _IA25Repository,
                                   IParametrosRepository _IParametrosRepository)
        {
            this._IA25Repository = _IA25Repository;
            this._IParametrosRepository = _IParametrosRepository;
        }

        public async Task EjecutarA25()
        {
            try
            {
                await ProcesarA25();
            }
            catch
            {
                throw;
            }
        }

        private async ValueTask ProcesarA25()
        {
            try
            {
                GeTparametros RutaParametro = await _IParametrosRepository.ConsultarParametroPorNombre("RUTA_PLANO_A25");

                if (!Directory.Exists(RutaParametro.Valor))
                    return;

                string RutaProcesados = string.Concat(RutaParametro.Valor.TrimEnd('\\'), @"\ProcesadosA25");

                if (!Directory.Exists(RutaProcesados))
                {
                    Directory.CreateDirectory(RutaProcesados);
                }

                string[] Archivos = Directory.GetFiles(RutaParametro.Valor);

                foreach (var Plano in Archivos)
                {
                    var Archivo = await LeerPlano(Plano);
                    var NombreArchivo = Path.GetFileName(Plano);

                    List<GeTa25> ListaA25 = new List<GeTa25>();

                    foreach (string line in Archivo)
                    {
                        string[] Fila = line.Split('\t');
                        string[] Hora = Fila[5].Split(' ');
                        string[] Fecha = Hora[0].Split('/');
                        string StrFecha = Fecha[2].ToString() + "-" + Fecha[1].ToString() + "-" + Fecha[0].ToString();
                        string StrTecnica = Fila[1].ToString().Trim();
                        string StrPaciente = Fila[0].ToString().Substring(0, 4).Trim();

                        GeTa25 a25 = new GeTa25()
                        {
                            Fecha = StrFecha,
                            Tecnica = StrTecnica,
                            IdPaciente = StrPaciente
                        };

                        var Existe = await ConsultarExiste(a25);

                        if (!Existe)
                        {
                            a25 = new GeTa25
                            {
                                IdPaciente = Fila[0].ToString().Substring(0, 4),
                                Tecnica = Fila[1].ToString(),
                                TipoMuestra = Fila[2].ToString(),
                                Resultado = Fila[3].ToString(),
                                Unidades = Fila[4].ToString(),
                                Fecha = StrFecha,
                                Hora = Hora[1].ToString(),
                                Sede = "-"
                            };
                            ListaA25.Add(a25);
                        }
                    }
                    await GuardarExamenesA25(ListaA25);
                    string RutaDestino = string.Concat(RutaProcesados, "\\", NombreArchivo);
                    File.Move(Plano, RutaDestino, true);
                }
            }
            catch
            {
                throw;
            }
        }

        private async ValueTask<string[]> LeerPlano(string Archivo)
        {
            string[] lines = await File.ReadAllLinesAsync(Archivo);
            return lines;
        }

        private async ValueTask GuardarExamenesA25(List<GeTa25> ListaA25)
        {
            await _IA25Repository.Guardar(ListaA25);
        }

        private async ValueTask<bool> ConsultarExiste(GeTa25 objA25)
        {
            return await _IA25Repository.ConsultarExiste(objA25);
        }
    }
}
