﻿using System;
using System.Collections.Generic;

namespace ApiServicio.Entidades
{
    public partial class GeTexamenesCargados
    {
        public int Id { get; set; }
        public string IdExamenSofia { get; set; }
        public string IdPaciente { get; set; }
        public string Tipo { get; set; }
        public string Muestra { get; set; }
        public int? IdProceso { get; set; }
        public int? IdCarga { get; set; }
    }
}
