﻿using System;
using System.Collections.Generic;

namespace ApiServicio.Entidades
{
    public partial class GeTprocesos
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime? Fecha { get; set; }
        public string Sede { get; set; }
        public string Descripcion { get; set; }
        public string Cargados { get; set; }
    }
}
