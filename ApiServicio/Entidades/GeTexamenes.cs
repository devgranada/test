﻿using System;
using System.Collections.Generic;

namespace ApiServicio.Entidades
{
    public partial class GeTexamenes
    {
        public int Id { get; set; }
        public string CodId { get; set; }
        public string ProtocoloCodigo { get; set; }
        public string ProtocoloTitulo { get; set; }
        public string Tubo { get; set; }
        public string TuboMuestra { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
        public string Paciente { get; set; }
        public string Nombre { get; set; }
        public string Sexo { get; set; }
        public string Edad { get; set; }
        public string FechaNacimiento { get; set; }
        public string PacienteId { get; set; }
        public bool? Hemograma { get; set; }
        public string Sede { get; set; }
    }
}
