﻿using System;
using System.Collections.Generic;

namespace ApiServicio.Entidades
{
    public partial class GeTrayto
    {
        public int Id { get; set; }
        public string Paciente { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
        public string Wbc { get; set; }
        public string Lym1 { get; set; }
        public string Mid1 { get; set; }
        public string Gra1 { get; set; }
        public string Lym2 { get; set; }
        public string Mid2 { get; set; }
        public string Gra2 { get; set; }
        public string Rbc { get; set; }
        public string Hgb { get; set; }
        public string Mchc { get; set; }
        public string Mch { get; set; }
        public string Mcv { get; set; }
        public string RdwCv { get; set; }
        public string RdwSd { get; set; }
        public string Hct { get; set; }
        public string Plt { get; set; }
        public string Mpv { get; set; }
        public string Pdw { get; set; }
        public string Pct { get; set; }
        public string PLcr { get; set; }
        public int? Estado { get; set; }
        public string Sede { get; set; }
    }
}
