﻿using System;
using System.Collections.Generic;

namespace ApiServicio.Entidades
{
    public partial class GeTa25
    {
        public int Id { get; set; }
        public string IdPaciente { get; set; }
        public string Tecnica { get; set; }
        public string TipoMuestra { get; set; }
        public string Resultado { get; set; }
        public string Unidades { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
        public string Sede { get; set; }
    }
}
