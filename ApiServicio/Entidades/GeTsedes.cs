﻿using System;
using System.Collections.Generic;

namespace ApiServicio.Entidades
{
    public partial class GeTsedes
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string ApiKey { get; set; }
        public string ApiSecret { get; set; }
        public string ApiRedirectUri { get; set; }
        public int? Estado { get; set; }
    }
}
