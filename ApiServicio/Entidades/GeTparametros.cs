﻿using System;
using System.Collections.Generic;

namespace ApiServicio.Entidades
{
    public partial class GeTparametros
    {
        public int Id { get; set; }
        public string Llave { get; set; }
        public string Valor { get; set; }
        public string ValorInfo { get; set; }
        public int Estado { get; set; }
        public string Descripcion { get; set; }
    }
}
