﻿using System;
using System.Collections.Generic;

namespace ApiServicio.Entidades
{
    public partial class GeTexamenesA25
    {
        public int Id { get; set; }
        public string CodA25 { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
        public bool? Hemograma { get; set; }
    }
}
