﻿using System;
using System.Collections.Generic;

namespace ApiServicio.Entidades
{
    public partial class GeTcargas
    {
        public int Id { get; set; }
        public string Cadena { get; set; }
        public string Fecha { get; set; }
        public string Respuesta { get; set; }
        public string Hora { get; set; }
        public int? IdProceso { get; set; }
        public int? IdExamen { get; set; }
        public string IdPaciente { get; set; }
    }
}
