﻿using System;
using System.Collections.Generic;

namespace ApiServicio.Entidades
{
    public partial class ItemExamenlab
    {
        public int Id { get; set; }
        public int? IdCabecera { get; set; }
        public string IdExamen { get; set; }
        public string Paciente { get; set; }
        public string Fecha { get; set; }
        public string Texto { get; set; }
        public string ValorCualitativo { get; set; }
        public string ValorReferencia { get; set; }
        public string ValorAdicional { get; set; }

        public virtual ActExamenlabFecha IdCabeceraNavigation { get; set; }
    }
}
