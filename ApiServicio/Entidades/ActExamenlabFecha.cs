﻿using System;
using System.Collections.Generic;

namespace ApiServicio.Entidades
{
    public partial class ActExamenlabFecha
    {
        public ActExamenlabFecha()
        {
            ItemExamenlab = new HashSet<ItemExamenlab>();
        }

        public int Id { get; set; }
        public string IdExamen { get; set; }
        public string Paciente { get; set; }
        public string Fecha { get; set; }
        public string ResultadoGlobal { get; set; }
        public string Responsable { get; set; }
        public string Nota { get; set; }
        public string Cargado { get; set; }
        public string FechaCarga { get; set; }
        public int? Proceso { get; set; }
        public DateTime? FechaProceso { get; set; }

        public virtual ICollection<ItemExamenlab> ItemExamenlab { get; set; }
    }
}
