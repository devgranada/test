﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using ApiServicio.Entidades;
using ApiServicio.Dto;

namespace ApiServicio.ContextoBD
{
    public partial class ApiContext : DbContext
    {
        public ApiContext()
        {
        }

        public ApiContext(DbContextOptions<ApiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ActExamenlabFecha> ActExamenlabFecha { get; set; }
        public virtual DbSet<GeTa25> GeTa25 { get; set; }
        public virtual DbSet<GeTcargas> GeTcargas { get; set; }
        public virtual DbSet<GeTexamenes> GeTexamenes { get; set; }
        public virtual DbSet<GeTexamenesA25> GeTexamenesA25 { get; set; }
        public virtual DbSet<GeTexamenesCargados> GeTexamenesCargados { get; set; }
        public virtual DbSet<GeTparametros> GeTparametros { get; set; }
        public virtual DbSet<GeTprocesos> GeTprocesos { get; set; }
        public virtual DbSet<GeTrayto> GeTrayto { get; set; }
        public virtual DbSet<GeTsedes> GeTsedes { get; set; }
        public virtual DbSet<ItemExamenlab> ItemExamenlab { get; set; }
        public DbQuery<DTOExamProcesados> ExamProcesados { get; set; }
        public DbQuery<DTORayto> Rayto { get; set; }
        public DbQuery<DTOHemograma> Hemograma { get; set; }
        public DbQuery<DTOProcesadosA25> DTOProcesadosA25 { get; set; }
        public DbQuery<DTOExamenesPro> DTOExamenesPro { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=FA-PTD014\\SQLEXPRESS;Initial Catalog=TianLab;Persist Security Info=False;User ID=tian;Password=tian2018+;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActExamenlabFecha>(entity =>
            {
                entity.ToTable("ACT_EXAMENLAB_FECHA");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Cargado)
                    .HasColumnName("cargado")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Fecha)
                    .HasColumnName("fecha")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FechaCarga)
                    .HasColumnName("fecha_carga")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.FechaProceso)
                    .HasColumnName("fechaProceso")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdExamen)
                    .HasColumnName("idExamen")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Nota)
                    .HasColumnName("nota")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Paciente)
                    .HasColumnName("paciente")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Proceso).HasColumnName("proceso");

                entity.Property(e => e.Responsable)
                    .HasColumnName("responsable")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ResultadoGlobal)
                    .HasColumnName("resultado_global")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GeTa25>(entity =>
            {
                entity.ToTable("GE_TA25");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Fecha)
                    .HasColumnName("fecha")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Hora)
                    .HasColumnName("hora")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.IdPaciente)
                    .HasColumnName("idPaciente")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Resultado)
                    .HasColumnName("resultado")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Sede)
                    .HasColumnName("sede")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Tecnica)
                    .HasColumnName("tecnica")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoMuestra)
                    .HasColumnName("tipoMuestra")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Unidades)
                    .HasColumnName("unidades")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GeTcargas>(entity =>
            {
                entity.ToTable("GE_TCARGAS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Cadena).IsUnicode(false);

                entity.Property(e => e.Fecha)
                    .HasColumnName("fecha")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Hora)
                    .HasColumnName("hora")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.IdExamen).HasColumnName("idExamen");

                entity.Property(e => e.IdPaciente)
                    .HasColumnName("idPaciente")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdProceso).HasColumnName("idProceso");

                entity.Property(e => e.Respuesta)
                    .HasColumnName("respuesta")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GeTexamenes>(entity =>
            {
                entity.ToTable("GE_TEXAMENES");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CodId)
                    .HasColumnName("codId")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Edad)
                    .HasColumnName("edad")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.Fecha)
                    .HasColumnName("fecha")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FechaNacimiento)
                    .HasColumnName("fecha_nacimiento")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Hemograma).HasColumnName("hemograma");

                entity.Property(e => e.Hora)
                    .HasColumnName("hora")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Paciente)
                    .HasColumnName("paciente")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PacienteId)
                    .HasColumnName("paciente_id")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.ProtocoloCodigo)
                    .HasColumnName("protocolo_codigo")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProtocoloTitulo)
                    .HasColumnName("protocolo_titulo")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Sede)
                    .HasColumnName("sede")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sexo)
                    .HasColumnName("sexo")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Tubo)
                    .HasColumnName("tubo")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.TuboMuestra)
                    .HasColumnName("tubo_muestra")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GeTexamenesA25>(entity =>
            {
                entity.ToTable("GE_TEXAMENES_A25");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CodA25)
                    .HasColumnName("codA25")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .HasColumnName("descripcion")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Hemograma).HasColumnName("hemograma");
            });

            modelBuilder.Entity<GeTexamenesCargados>(entity =>
            {
                entity.ToTable("GE_TEXAMENES_CARGADOS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdCarga).HasColumnName("idCarga");

                entity.Property(e => e.IdExamenSofia)
                    .HasColumnName("idExamenSofia")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdPaciente)
                    .HasColumnName("idPaciente")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdProceso).HasColumnName("idProceso");

                entity.Property(e => e.Muestra)
                    .HasColumnName("muestra")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo)
                    .HasColumnName("tipo")
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GeTparametros>(entity =>
            {
                entity.ToTable("GE_TPARAMETROS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("descripcion")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.Llave)
                    .IsRequired()
                    .HasColumnName("llave")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Valor)
                    .IsRequired()
                    .HasColumnName("valor")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ValorInfo)
                    .HasColumnName("valor_info")
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GeTprocesos>(entity =>
            {
                entity.ToTable("GE_TPROCESOS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Cargados)
                    .HasColumnName("cargados")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .HasColumnName("descripcion")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Fecha)
                    .HasColumnName("fecha")
                    .HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sede)
                    .HasColumnName("sede")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GeTrayto>(entity =>
            {
                entity.ToTable("GE_TRAYTO");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Fecha)
                    .HasColumnName("fecha")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Gra1)
                    .HasColumnName("gra1")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Gra2)
                    .HasColumnName("gra2")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Hct)
                    .HasColumnName("hct")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Hgb)
                    .HasColumnName("hgb")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Hora)
                    .HasColumnName("hora")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Lym1)
                    .HasColumnName("lym1")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Lym2)
                    .HasColumnName("lym2")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Mch)
                    .HasColumnName("mch")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Mchc)
                    .HasColumnName("mchc")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Mcv)
                    .HasColumnName("mcv")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Mid1)
                    .HasColumnName("mid1")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Mid2)
                    .HasColumnName("mid2")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Mpv)
                    .HasColumnName("mpv")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PLcr)
                    .HasColumnName("p_lcr")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Paciente)
                    .HasColumnName("paciente")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Pct)
                    .HasColumnName("pct")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Pdw)
                    .HasColumnName("pdw")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Plt)
                    .HasColumnName("plt")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Rbc)
                    .HasColumnName("rbc")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RdwCv)
                    .HasColumnName("rdw_cv")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RdwSd)
                    .HasColumnName("rdw_sd")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Sede)
                    .HasColumnName("sede")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Wbc)
                    .HasColumnName("wbc")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GeTsedes>(entity =>
            {
                entity.ToTable("GE_TSEDES");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ApiKey)
                    .HasColumnName("apiKey")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ApiRedirectUri)
                    .HasColumnName("apiRedirectURI")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ApiSecret)
                    .HasColumnName("apiSecret")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ItemExamenlab>(entity =>
            {
                entity.ToTable("ITEM_EXAMENLAB");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Fecha)
                    .HasColumnName("fecha")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.IdCabecera).HasColumnName("id_cabecera");

                entity.Property(e => e.IdExamen)
                    .HasColumnName("idExamen")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Paciente)
                    .HasColumnName("paciente")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Texto)
                    .HasColumnName("texto")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ValorAdicional)
                    .HasColumnName("valor_adicional")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ValorCualitativo)
                    .HasColumnName("valor_cualitativo")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ValorReferencia)
                    .HasColumnName("valor_referencia")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdCabeceraNavigation)
                    .WithMany(p => p.ItemExamenlab)
                    .HasForeignKey(d => d.IdCabecera)
                    .HasConstraintName("FK_ITEM_EXAMENLAB_ACT_EXAMENLAB_FECHA");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
