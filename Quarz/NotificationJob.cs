﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication4.Entities;
using WebApplication4.Logic;

namespace WebApplication4
{
    [DisallowConcurrentExecution]
    public class NotificationJob : IJob
    {
        //private readonly ILogger<NotificationJob> _logger;
        private readonly ILogger _logger;
        private readonly ITest _context;
        private int i;

        public NotificationJob(ILogger<NotificationJob> logger, ITest _context)
        {
            this._logger = logger;
            this._context = _context;
        }

        public Task Execute(IJobExecutionContext context)
        {
            i += 1;
            _logger.LogInformation("Hello world!");
            test obj = new test
            {
                id = i,
                nombre = "prueba",
                contador = i.ToString()
            };
            _logger.LogInformation(obj.id, " " + obj.nombre, " " + obj.contador);
            //this._context.guardar(obj);
            return Task.CompletedTask;
        }
    }
}
