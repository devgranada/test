﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication4.Entities;
using WebApplication4.Model;

namespace WebApplication4.Logic
{
    public class CTest : ITest
    {
        private readonly AppDbContext _context;

        public CTest(AppDbContext _context)
        {
            this._context = _context;
        }

        public async ValueTask guardar(test obj)
        {
            this._context.test.AddRange(obj);
            await _context.SaveChangesAsync();
        }
    }
}
